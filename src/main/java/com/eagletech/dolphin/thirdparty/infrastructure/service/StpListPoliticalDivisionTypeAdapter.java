package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListPoliticalDivisionTypeService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListPoliticalDivisionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListPoliticalDivisionTypeAdapter implements StpListPoliticalDivisionTypeService {

    @Autowired
    StpListPoliticalDivisionTypeRepository repository;

    @Override
    public List<StpListPoliticalDivisionTypeEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StpListPoliticalDivisionTypeEntity> findAllWithPagination(int offset, int pageSize) {
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    public Optional<StpListPoliticalDivisionTypeEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpListPoliticalDivisionTypeEntity save(StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity) {
        return repository.save(stpListPoliticalDivisionTypeEntity);
    }
}
