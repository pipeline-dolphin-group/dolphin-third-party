package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpPoliticalDivisionService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpPoliticalDivisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpPoliticalDivisionServiceAdapter implements StpPoliticalDivisionService {

    @Autowired
    private StpPoliticalDivisionRepository repository;

    @Override
    public List<StpPoliticalDivisionEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StpPoliticalDivisionEntity> findAllWithPagination(int offset, int pageSize) {
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    public Optional<StpPoliticalDivisionEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpPoliticalDivisionEntity save(StpPoliticalDivisionEntity stpPoliticalDivisionEntity) {
        return repository.save(stpPoliticalDivisionEntity);
    }

    @Override
    public List<StpPoliticalDivisionEntity> saveAll(List<StpPoliticalDivisionEntity> stpPoliticalDivisionEntities) {
        return repository.saveAll(stpPoliticalDivisionEntities);
    }
}
