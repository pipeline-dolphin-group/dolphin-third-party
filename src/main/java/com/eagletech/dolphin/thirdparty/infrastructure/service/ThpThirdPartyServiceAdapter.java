package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import com.eagletech.dolphin.thirdparty.domain.service.ThpThirdPartyService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.ThpThirdPartyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ThpThirdPartyServiceAdapter implements ThpThirdPartyService {

    @Autowired
    private ThpThirdPartyRepository repository;

    @Override
    public List<ThpThirdPartyEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<ThpThirdPartyEntity> findAllWithPagination(int offset, int pageSize) {
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    public List<ThpThirdPartyEntity> findByNumberIdentificationOrSurname(String param) {
        return repository.findByNumberIdentificationOrSurname(param);
    }

    @Override
    public Optional<ThpThirdPartyEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public ThpThirdPartyEntity save(ThpThirdPartyEntity thirdParty) {
        return repository.save(thirdParty);
    }

    @Override
    public List<ThpThirdPartyEntity> saveAll(List<ThpThirdPartyEntity> thirdParties) {
        return repository.saveAll(thirdParties);
    }
}
