package com.eagletech.dolphin.thirdparty.infrastructure.repository;

import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThpThirdPartyRepository extends JpaRepository<ThpThirdPartyEntity, Long> {

    @Query(
            value = "SELECT * FROM \n" +
                    "thp_thirdparty t\n" +
                    "WHERE t.number_identification LIKE :parameter OR t.first_name = :parameter OR t.surname LIKE :parameter OR middle_name LIKE :parameter OR second_surname LIKE :parameter",
            nativeQuery = true
    )
    List<ThpThirdPartyEntity> findByNumberIdentificationOrSurname(@Param("parameter")  String parameter);

    ThpThirdPartyEntity findByNumberIdentification(String identification);
}
