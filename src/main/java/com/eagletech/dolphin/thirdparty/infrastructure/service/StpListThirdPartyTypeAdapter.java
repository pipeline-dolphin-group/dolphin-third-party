package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListThirdPartyTypeService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListThirdPartyTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListThirdPartyTypeAdapter implements StpListThirdPartyTypeService {

    @Autowired
    private StpListThirdPartyTypeRepository repository;

    @Override
    public List<StpListThirdPartyTypeEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StpListThirdPartyTypeEntity> findAllWithPagination(int offset, int pageSize) {
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    public Optional<StpListThirdPartyTypeEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpListThirdPartyTypeEntity save(StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity) {
        return repository.save(stpListThirdPartyTypeEntity);
    }
}
