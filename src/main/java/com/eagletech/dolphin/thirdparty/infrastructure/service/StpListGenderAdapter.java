package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListGenderService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListGenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListGenderAdapter implements StpListGenderService {

    @Autowired
    StpListGenderRepository repository;

    @Override
    public List<StpListGenderEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<StpListGenderEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpListGenderEntity save(StpListGenderEntity stpListGenderEntity) {
        return repository.save(stpListGenderEntity);
    }
}
