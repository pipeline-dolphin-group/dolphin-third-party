package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListPoliticalDivisionGroupService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListPoliticalDivisionGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListPoliticalDivisionGroupAdapter implements StpListPoliticalDivisionGroupService {

    @Autowired
    StpListPoliticalDivisionGroupRepository repository;

    @Override
    public List<StpListPoliticalDivisionGroupEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<StpListPoliticalDivisionGroupEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpListPoliticalDivisionGroupEntity save(StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity) {
        return repository.save(stpListPoliticalDivisionGroupEntity);
    }
}
