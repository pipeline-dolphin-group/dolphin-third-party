package com.eagletech.dolphin.thirdparty.infrastructure.repository;

import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StpListTypeIdentificationRepository extends JpaRepository<StpListTypeIdentificationEntity, Long> {
}
