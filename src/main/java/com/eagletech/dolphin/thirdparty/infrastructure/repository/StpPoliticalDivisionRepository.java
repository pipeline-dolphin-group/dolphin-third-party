package com.eagletech.dolphin.thirdparty.infrastructure.repository;

import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StpPoliticalDivisionRepository extends JpaRepository<StpPoliticalDivisionEntity, Long> {
}
