package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListLegalNatureService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListLegalNatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListLegalNatureAdapter implements StpListLegalNatureService {

    @Autowired
    StpListLegalNatureRepository repository;

    @Override
    public List<StpListLegalNatureEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<StpListLegalNatureEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public StpListLegalNatureEntity save(StpListLegalNatureEntity stpListLegalNatureEntity) {
        return repository.save(stpListLegalNatureEntity);
    }
}
