package com.eagletech.dolphin.thirdparty.infrastructure.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import com.eagletech.dolphin.thirdparty.domain.service.StpListTypeIdentificationService;
import com.eagletech.dolphin.thirdparty.infrastructure.repository.StpListTypeIdentificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StpListTypeIdentificationServiceAdapter implements StpListTypeIdentificationService {

    @Autowired
    private StpListTypeIdentificationRepository repository;

    @Override
    public List<StpListTypeIdentificationEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StpListTypeIdentificationEntity> findAllWithPagination(int offset, int pageSize) {
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    public Optional<StpListTypeIdentificationEntity> findById(Long id) {
        return  repository.findById(id);
    }

    @Override
    public StpListTypeIdentificationEntity save(StpListTypeIdentificationEntity stpPoliticalDivisionEntity) {
        return repository.save(stpPoliticalDivisionEntity);
    }

    @Override
    public List<StpListTypeIdentificationEntity> saveAll(List<StpListTypeIdentificationEntity> stpPoliticalDivisionEntities) {
        return repository.saveAll(stpPoliticalDivisionEntities);
    }
}
