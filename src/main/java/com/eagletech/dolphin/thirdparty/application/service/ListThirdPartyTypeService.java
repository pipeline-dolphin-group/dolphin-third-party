package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListThirdPartyTypeDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ListThirdPartyTypeService {

    List<StpListThirdPartyTypeEntity> getListThirdPartyType();

    Page<StpListThirdPartyTypeEntity> findAllWithPagination(int offset, int pageSize);

    StpListThirdPartyTypeDto saveStpListThirdPartyType(StpListThirdPartyTypeDto stpListThirdPartyTypeDto);

    StpListThirdPartyTypeDto updateStpListThirdPartyType(StpListThirdPartyTypeDto stpListThirdPartyTypeDto);
}
