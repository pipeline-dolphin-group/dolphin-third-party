package com.eagletech.dolphin.thirdparty.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StpListPoliticalDivisionTypeDto {

    private Long idListPoliticalDivisionType;
    private String code;
    private String name;
    private Boolean state;
    private Long idUserLog;
    private Date datetimeLog;
}
