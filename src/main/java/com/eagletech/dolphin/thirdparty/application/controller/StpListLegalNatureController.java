package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;
import com.eagletech.dolphin.thirdparty.application.service.ListLegalNatureService;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RequestMapping("/third-party-api/legal-nature")
@RestController
@Validated
public class StpListLegalNatureController {

    private final ListLegalNatureService service;

    private Util util = new Util();

    @Autowired
    public StpListLegalNatureController(ListLegalNatureService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Legal Nature",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Legal Nature")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListLegalNatureDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAllLegalNature() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getListLegalNatures());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Legal Nature",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Legal Nature")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListLegalNatureDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<Object> addStpListLegalNature(@RequestBody StpListLegalNatureDto stpListLegalNatureDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListLegalNature(stpListLegalNatureDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Legal Nature",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Legal Nature")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListLegalNatureDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<Object> updateStpListLegalNature(@RequestBody StpListLegalNatureDto stpListLegalNatureDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListLegalNature(stpListLegalNatureDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
