package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListGenderDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;

import java.util.List;

public interface ListGenderService {

    List<StpListGenderEntity> getListGenders();

    StpListGenderDto saveStpListGender(StpListGenderDto stpListGenderDto);

    StpListGenderDto updateStpListGender(StpListGenderDto stpListGenderDto);
}
