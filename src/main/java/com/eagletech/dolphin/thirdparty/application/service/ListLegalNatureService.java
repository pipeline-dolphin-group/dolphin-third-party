package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;

import java.util.List;

public interface ListLegalNatureService {

    List<StpListLegalNatureEntity> getListLegalNatures();

    StpListLegalNatureDto saveStpListLegalNature(StpListLegalNatureDto stpListLegalNatureDto);

    StpListLegalNatureDto updateStpListLegalNature(StpListLegalNatureDto stpListLegalNatureDto);

}
