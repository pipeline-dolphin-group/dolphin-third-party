package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListGenderDto;
import com.eagletech.dolphin.thirdparty.application.service.ListGenderService;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RequestMapping("/third-party-api/gender")
@RestController
@Validated
public class StpListGenderController {

    private final ListGenderService service;

    private Util util = new Util();

    @Autowired
    public StpListGenderController(ListGenderService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Gender",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Gender")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListGenderDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAllGenders() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getListGenders());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Gender",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Gender")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListGenderDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<Object> addGenders(@RequestBody StpListGenderDto stpListGenderDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListGender(stpListGenderDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Gender",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Gender")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListGenderDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<Object> updateGenders(@RequestBody StpListGenderDto stpListGenderDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListGender(stpListGenderDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
