package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.ThpThirdPartyDto;
import com.eagletech.dolphin.thirdparty.application.service.ThirdPartyService;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import com.eagletech.dolphin.thirdparty.domain.util.Error;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.List;

@RestController
@Validated
@RequestMapping("/third-party-api/third-party")
public class StpThirdPartyController {

    private final ThirdPartyService service;

    private final Util util = new Util();

    @Autowired
    public StpThirdPartyController(ThirdPartyService service) {
        this.service = service;
    }

    @Operation(summary = "Get All ThirdParty",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All ThirdParty")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<List<ThpThirdPartyEntity>> findAllThirdParties() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getThirdParty());
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get ThirdParty By Id",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get ThirdParty By Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/getById/{id}")
    public ResponseEntity<ThpThirdPartyEntity> findThirdPartyById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getThirdPartyById(id));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get All ThirdParty Paginated",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All ThirdParty Paginated")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll/{offset}/{pageSize}")
    public ResponseEntity<Object> findAllThirdPartiesWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findAllWithPagination(offset, pageSize));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Find By Name Or Identification ThirdParty",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Find By Name Or Identification ThirdParty")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findByNameOrIdentification/{param}")
    public ResponseEntity<List<ThpThirdPartyEntity>> findByNameOrIdentification(@PathVariable String param) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findByNameOrIdentification(param));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "create ThirdParty",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "create ThirdParty")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<ThpThirdPartyDto> addThirdParty(@RequestBody ThpThirdPartyDto thirdPartyDto) {
        try {
            return new ResponseEntity<>(service.saveThirdParty(thirdPartyDto), HttpStatus.CREATED);
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Import ThirdParty",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Import ThirdParty")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/importThirdParties")
    public ResponseEntity<List<ThpThirdPartyDto>> addThirdParties(@RequestBody List<ThpThirdPartyDto> thirdPartiesDtos) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveThirdParties(thirdPartiesDtos));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update ThirdParty",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update ThirdParty")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ThpThirdPartyDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<ThpThirdPartyDto> updateThirdParty(@RequestBody ThpThirdPartyDto thirdPartyDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateThirdParty(thirdPartyDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

}
