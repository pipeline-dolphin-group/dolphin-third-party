package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionGroupDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;

import java.util.List;

public interface ListPoliticalDivisionGroupService {

    List<StpListPoliticalDivisionGroupEntity> getStpListPoliticalDivisionGroups();

    StpListPoliticalDivisionGroupDto saveStpListPoliticalDivisionGroup(StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto);

    StpListPoliticalDivisionGroupDto updateStpListPoliticalDivisionGroup(StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto);

}
