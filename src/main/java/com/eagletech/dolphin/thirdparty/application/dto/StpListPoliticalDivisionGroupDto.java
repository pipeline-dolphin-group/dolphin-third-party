package com.eagletech.dolphin.thirdparty.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StpListPoliticalDivisionGroupDto {

    private long idListPoliticalDivisionGroup;
    private String code;
    private String name;
    private Boolean state;
    private long idUserLog;
    private Date datetimeLog;
}
