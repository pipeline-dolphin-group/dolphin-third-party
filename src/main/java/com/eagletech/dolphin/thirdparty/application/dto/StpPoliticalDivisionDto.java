package com.eagletech.dolphin.thirdparty.application.dto;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StpPoliticalDivisionDto {

    private long idPoliticalDivision;
    private Long idPoliticalDivisionFather;
    private Long idListPoliticalDivisionType;
    private Long idListPoliticalDivisionGroup;
    private String code;
    private String name;
    private boolean state;
    private Long idUserLog;
    private Date datetimeLog;
    private StpPoliticalDivisionEntity politicalDivisionFather;
    private StpListPoliticalDivisionTypeEntity politicalDivisionType;
    private StpListPoliticalDivisionGroupEntity listPoliticalDivisionGroup;
}
