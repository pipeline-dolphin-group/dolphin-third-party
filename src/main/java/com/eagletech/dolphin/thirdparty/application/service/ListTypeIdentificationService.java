package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListTypeIdentificationDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ListTypeIdentificationService {

    List<StpListTypeIdentificationEntity> getListTypeIdentifications();

    Page<StpListTypeIdentificationEntity> findAllWithPagination(int offset, int pageSize);

    StpListTypeIdentificationEntity getListTypeIdentificationById(Long id);

    StpListTypeIdentificationDto saveStpListTypeIdentification(StpListTypeIdentificationDto listTypeIdentification);

    StpListTypeIdentificationDto updateStpListTypeIdentification(StpListTypeIdentificationDto listTypeIdentification);
}
