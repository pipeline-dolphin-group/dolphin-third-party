package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionTypeDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;

import java.util.List;

public interface ListPoliticalDivisionTypeService {

    List<StpListPoliticalDivisionTypeEntity> getStpListPoliticalDivisionTypes();

    StpListPoliticalDivisionTypeDto saveStpListPoliticalDivisionType(StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto);

    StpListPoliticalDivisionTypeDto updateStpListPoliticalDivisionType(StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto);

}
