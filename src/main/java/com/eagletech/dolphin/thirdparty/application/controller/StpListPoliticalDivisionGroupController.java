package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionGroupDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionGroupService;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RequestMapping("/third-party-api/political-division-group")
@RestController
@Validated
public class StpListPoliticalDivisionGroupController {

    private ListPoliticalDivisionGroupService service;

    private Util util = new Util();

    @Autowired
    public StpListPoliticalDivisionGroupController(ListPoliticalDivisionGroupService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Political Division Group",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Political Division Group")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionGroupDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAllStpListPoliticalDivisionGroup() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getStpListPoliticalDivisionGroups());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Political Division Group",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Political Division Group")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionGroupDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<Object> addStpListPoliticalDivisionGroup(@RequestBody StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Political Division Group",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Updtae Political Division Group")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionGroupDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<Object> updateStpListPoliticalDivisionGroup(@RequestBody StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
