package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpPoliticalDivisionDto;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PoliticalDivisionService {

    List<StpPoliticalDivisionEntity> getPoliticalDivision();

    Page<StpPoliticalDivisionEntity> findAllWithPagination(int offset, int pageSize);

    StpPoliticalDivisionEntity getPoliticalDivisionById(Long id);

    StpPoliticalDivisionDto savePoliticalDivision(StpPoliticalDivisionDto stpPoliticalDivisionDto);

    List<StpPoliticalDivisionDto> savePoliticalDivisions(List<StpPoliticalDivisionDto> stpPoliticalDivisionsDto);

    StpPoliticalDivisionDto updatePoliticalDivision(StpPoliticalDivisionDto stpPoliticalDivisionDto);
}
