package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpPoliticalDivisionDto;
import com.eagletech.dolphin.thirdparty.application.service.PoliticalDivisionService;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.List;

@RestController
@Validated
@RequestMapping("/third-party-api/political-division")
public class StpPoliticalDivisionController {

    private PoliticalDivisionService service;

    private Util util = new Util();

    @Autowired
    public StpPoliticalDivisionController(PoliticalDivisionService service) {
        this.service = service;
    }

    @Operation(summary = "Get All PoliticalDivision",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All PoliticalDivision")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<List<StpPoliticalDivisionEntity>> findAllPoliticalDivision() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getPoliticalDivision());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get All Political Division Paginated",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Political Division Paginated")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll/{offset}/{pageSize}")
    public ResponseEntity<Page<StpPoliticalDivisionEntity>> findAllPoliticalDivisionWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findAllWithPagination(offset, pageSize));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get PoliticalDivision By Id",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get PoliticalDivision By Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/getById/{id}")
    public ResponseEntity<StpPoliticalDivisionEntity> findPoliticalDivisionById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getPoliticalDivisionById(id));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Political Division",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Political Division")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<StpPoliticalDivisionDto> addStpPoliticalDivision(@RequestBody StpPoliticalDivisionDto stpPoliticalDivisionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.savePoliticalDivision(stpPoliticalDivisionDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Import Political Division",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Import Political Division")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/importPoliticalDivision")
    public ResponseEntity<List<StpPoliticalDivisionDto>> addPoliticalDivisions(@RequestBody List<StpPoliticalDivisionDto> politicalDivisions) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.savePoliticalDivisions(politicalDivisions));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Political Division",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Political Division")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpPoliticalDivisionDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<StpPoliticalDivisionDto> updateStpPoliticalDivision(@RequestBody StpPoliticalDivisionDto stpPoliticalDivisionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updatePoliticalDivision(stpPoliticalDivisionDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
