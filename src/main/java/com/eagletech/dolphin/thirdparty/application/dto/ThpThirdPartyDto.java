package com.eagletech.dolphin.thirdparty.application.dto;

import com.eagletech.dolphin.thirdparty.domain.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThpThirdPartyDto {

    private Long idThirdParty;
    private String numberIdentification;
    private String surname;
    private String secondSurname;
    private String firstName;
    private String middleName;
    private Date birthdayDate;
    private String mobile;
    private String mail;
    private boolean state;
    private Long userLog;
    private Date datetimeLog;
    private String address;
    private Long idPoliticalDivisionCity;
    private Long idPoliticalDivisionNeighborhood;
    private Long idThirdPartyType;
    private Long idListLegalNature;
    private Long idListTypeIdentification;
    private Long idListGender;
    private StpPoliticalDivisionEntity politicalDivisionCity;
    private StpPoliticalDivisionEntity politicalDivisionNeighborhood;
    private StpListThirdPartyTypeEntity thirdPartyType;
    private StpListLegalNatureEntity listLegalNature;
    private StpListTypeIdentificationEntity listTypeIdentification;
    private StpListGenderEntity listGender;
}
