package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListTypeIdentificationDto;
import com.eagletech.dolphin.thirdparty.application.service.ListTypeIdentificationService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.List;

@RestController
@Validated
@RequestMapping("/third-party-api/type-identification")
public class StpListTypeIdentificationController {

    private ListTypeIdentificationService service;

    private Util util = new Util();

    @Autowired
    public StpListTypeIdentificationController(ListTypeIdentificationService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Type Identification",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Type Identification")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListTypeIdentificationDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<List<StpListTypeIdentificationEntity>> findAllTypeIdentification() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getListTypeIdentifications());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get Type Identification By Id",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get Type Identification By Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListTypeIdentificationDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/getById/{id}")
    public ResponseEntity<StpListTypeIdentificationEntity> findTypeIdentificationById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getListTypeIdentificationById(id));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get All Type Identification Paginated",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Type Identification Paginated")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListTypeIdentificationDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll/{offset}/{pageSize}")
    public ResponseEntity<Page<StpListTypeIdentificationEntity>> findAllTypeIdentificationWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findAllWithPagination(offset, pageSize));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Type Identification",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Type Identification")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListTypeIdentificationDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<StpListTypeIdentificationDto> addStpListTypeIdentification(@RequestBody StpListTypeIdentificationDto stpListTypeIdentificationDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListTypeIdentification(stpListTypeIdentificationDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Type Identification",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Type Identification")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListTypeIdentificationDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<StpListTypeIdentificationDto> updateStpListTypeIdentification(@RequestBody StpListTypeIdentificationDto stpListTypeIdentificationDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListTypeIdentification(stpListTypeIdentificationDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
