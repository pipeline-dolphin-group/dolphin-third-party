package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionTypeService;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RequestMapping("/third-party-api/political-division-type")
@RestController
@Validated
public class StpListPoliticalDivisionTypeController {

    private final ListPoliticalDivisionTypeService service;

    private final Util util = new Util();

    @Autowired
    public StpListPoliticalDivisionTypeController(ListPoliticalDivisionTypeService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Political Division Type",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Political Division Type")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAllStpListPoliticalDivisionType() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getStpListPoliticalDivisionTypes());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create All Political Division Type",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Political Division Type")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<Object> addStpListPoliticalDivisionType(@RequestBody StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListPoliticalDivisionType(stpListPoliticalDivisionTypeDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Political Division Type",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Political Division Type")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListPoliticalDivisionTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<Object> updateStpListPoliticalDivisionType(@RequestBody StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListPoliticalDivisionType(stpListPoliticalDivisionTypeDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
