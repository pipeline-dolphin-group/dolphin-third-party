package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListThirdPartyTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListThirdPartyTypeService;
import com.eagletech.dolphin.thirdparty.domain.util.Util;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RestController
@Validated
@RequestMapping("/third-party-api/third-party-type")
public class StpListThirdPartyTypeController {

    private ListThirdPartyTypeService service;

    private Util util = new Util();

    @Autowired
    public StpListThirdPartyTypeController(ListThirdPartyTypeService service) {
        this.service = service;
    }

    @Operation(summary = "Get All Third Party Types",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Third Party Types")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListThirdPartyTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAllThirdPartyTypes() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getListThirdPartyType());
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Get All Third Party Types Paginated",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Get All Third Party Types Paginated")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListThirdPartyTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @GetMapping("/findAll/{offset}/{pageSize}")
    public ResponseEntity<Object> findAllThirdPartyTypesWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findAllWithPagination(offset, pageSize));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Create Third Party Types",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Create Third Party Types")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListThirdPartyTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PostMapping("/create")
    public ResponseEntity<Object> addStpListThirdPartyType(@RequestBody StpListThirdPartyTypeDto stpListThirdPartyTypeDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.saveStpListThirdPartyType(stpListThirdPartyTypeDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @Operation(summary = "Update Third Party Types",
            security = {@SecurityRequirement(name = "BearerAuth")},
            description = "Update Third Party Types")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = StpListThirdPartyTypeDto.class),
            @ApiResponse(code = 400, message = "Bad request", response = Error.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
            @ApiResponse(code = 404, message = "Not Found", response = Error.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = Error.class),
            @ApiResponse(code = 500, message = "Internal server error", response = Error.class)
    })
    @PutMapping("/update")
    public ResponseEntity<Object> updateStpListThirdPartyType(@RequestBody StpListThirdPartyTypeDto stpListThirdPartyTypeDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateStpListThirdPartyType(stpListThirdPartyTypeDto));
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
