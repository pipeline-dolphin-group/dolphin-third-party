package com.eagletech.dolphin.thirdparty.application.service;

import com.eagletech.dolphin.thirdparty.application.dto.ThpThirdPartyDto;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ThirdPartyService {

    List<ThpThirdPartyEntity> getThirdParty();

    List<ThpThirdPartyEntity> findByNameOrIdentification(String param);

    Page<ThpThirdPartyEntity> findAllWithPagination(int offset, int pageSize);

    ThpThirdPartyEntity getThirdPartyById(Long id);

    ThpThirdPartyDto saveThirdParty(ThpThirdPartyDto thirdParty);

    List<ThpThirdPartyDto> saveThirdParties(List<ThpThirdPartyDto> thirdParties);

    ThpThirdPartyDto updateThirdParty(ThpThirdPartyDto thirdPartyDto);
}
