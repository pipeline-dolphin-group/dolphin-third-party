package com.eagletech.dolphin.thirdparty.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stp_list_gender")
public class StpListGenderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_list_gender")
    private long idListGender;
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "state")
    private Boolean state;
    @Column(name = "id_user_log")
    private long idUserLog;
    @Column(name = "datetime")
    private Date datetimeLog;
}
