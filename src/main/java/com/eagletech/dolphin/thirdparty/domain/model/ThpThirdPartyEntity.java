package com.eagletech.dolphin.thirdparty.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
        name = "thp_thirdparty",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"number_identification", "id_thirdparty"})
)
public class ThpThirdPartyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_thirdparty")
    private Long idThirdParty;
    @Size(min = 5, max = 10)
    @Column(name = "number_identification", unique = true)
    private String numberIdentification;
    @Column(name = "surname")
    private String surname;
    @Column(name = "second_surname")
    private String secondSurname;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @NotNull(message = "El campo birthday_date no puede ser nulo.")
    @Column(name = "birthday_date")
    private Date birthdayDate;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "mail", unique = true)
    @Email(message = "El formato del campo Email invalido.")
    private String mail;
    @Column(name = "state")
    private boolean state;
    @Column(name = "user_log")
    private Long userLog;
    @Column(name = "datetime_log")
    @NotNull(message = "El campo birthday_date no puede ser nulo.")
    private Date datetimeLog;
    @Column(name = "address")
    private String address;
    @Column(name = "id_political_division_city")
    private Long idPoliticalDivisionCity;
    @Column(name = "id_political_division_neighborhood")
    private Long idPoliticalDivisionNeighborhood;
    @Column(name = "id_list_thirdparty_type")
    private Long idThirdPartyType;
    @Column(name = "id_list_legal_nature")
    private Long idListLegalNature;
    @Column(name = "id_list_type_identification")
    private Long idListTypeIdentification;
    @Column(name = "id_list_gender")
    private Long idListGender;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_political_division_city", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpPoliticalDivisionEntity politicalDivisionCity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_political_division_neighborhood", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpPoliticalDivisionEntity politicalDivisionNeighborhood;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_thirdparty_type", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListThirdPartyTypeEntity thirdPartyType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_list_legal_nature", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListLegalNatureEntity listLegalNature;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_list_type_identification", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListTypeIdentificationEntity listTypeIdentification;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_list_gender", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListGenderEntity listGender;
}
