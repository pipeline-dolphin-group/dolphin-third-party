package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.ThpThirdPartyDto;

public class ThpThirdPartyBuilder {

    private ThpThirdPartyBuilder() {
    }

    /**
     * Description: This method convert a third party entity to third party dto.
     *
     * @param thpThirdPartyEntity This param is to separate the business logic to the persistence layer.
     * @return ThpThirdPartyDto This return a third party entity converting to dto.
     */
    public static ThpThirdPartyDto convertToDomain(ThpThirdPartyEntity thpThirdPartyEntity) {
        ThpThirdPartyDto thpThirdPartyDto = null;
        if (thpThirdPartyEntity != null) {
            thpThirdPartyDto = new ThpThirdPartyDto(
                    thpThirdPartyEntity.getIdThirdParty(),
                    thpThirdPartyEntity.getNumberIdentification(),
                    thpThirdPartyEntity.getSurname(),
                    thpThirdPartyEntity.getSecondSurname(),
                    thpThirdPartyEntity.getFirstName(),
                    thpThirdPartyEntity.getMiddleName(),
                    thpThirdPartyEntity.getBirthdayDate(),
                    thpThirdPartyEntity.getMobile(),
                    thpThirdPartyEntity.getMail(),
                    thpThirdPartyEntity.isState(),
                    thpThirdPartyEntity.getUserLog(),
                    thpThirdPartyEntity.getDatetimeLog(),
                    thpThirdPartyEntity.getAddress(),
                    thpThirdPartyEntity.getIdPoliticalDivisionCity(),
                    thpThirdPartyEntity.getIdPoliticalDivisionNeighborhood(),
                    thpThirdPartyEntity.getIdThirdPartyType(),
                    thpThirdPartyEntity.getIdListLegalNature(),
                    thpThirdPartyEntity.getIdListTypeIdentification(),
                    thpThirdPartyEntity.getIdListGender(),
                    thpThirdPartyEntity.getPoliticalDivisionCity(),
                    thpThirdPartyEntity.getPoliticalDivisionNeighborhood(),
                    thpThirdPartyEntity.getThirdPartyType(),
                    thpThirdPartyEntity.getListLegalNature(),
                    thpThirdPartyEntity.getListTypeIdentification(),
                    thpThirdPartyEntity.getListGender()
            );
        }
        return thpThirdPartyDto;
    }

    /**
     * Description: This method convert a third party dto to third party entity.
     *
     * @param thpThirdPartyDto This param is to separate the business logic to the persistence layer.
     * @return ThpThirdPartyEntity This return a third party dto converting to entity.
     */
    public static ThpThirdPartyEntity convertToEntity(ThpThirdPartyDto thpThirdPartyDto) {
        ThpThirdPartyEntity thpThirdPartyEntity = new ThpThirdPartyEntity();
        thpThirdPartyEntity.setIdThirdParty(thpThirdPartyDto.getIdThirdParty());
        thpThirdPartyEntity.setNumberIdentification(thpThirdPartyDto.getNumberIdentification());
        thpThirdPartyEntity.setSurname(thpThirdPartyDto.getSurname());
        thpThirdPartyEntity.setSecondSurname(thpThirdPartyDto.getSecondSurname());
        thpThirdPartyEntity.setFirstName(thpThirdPartyDto.getFirstName());
        thpThirdPartyEntity.setMiddleName(thpThirdPartyDto.getMiddleName());
        thpThirdPartyEntity.setBirthdayDate(thpThirdPartyDto.getBirthdayDate());
        thpThirdPartyEntity.setMobile(thpThirdPartyDto.getMobile());
        thpThirdPartyEntity.setMail(thpThirdPartyDto.getMail());
        thpThirdPartyEntity.setState(thpThirdPartyDto.isState());
        thpThirdPartyEntity.setUserLog(thpThirdPartyDto.getUserLog());
        thpThirdPartyEntity.setDatetimeLog(thpThirdPartyDto.getDatetimeLog());
        thpThirdPartyEntity.setAddress(thpThirdPartyDto.getAddress());
        thpThirdPartyEntity.setIdPoliticalDivisionCity(thpThirdPartyDto.getIdPoliticalDivisionCity());
        thpThirdPartyEntity.setIdPoliticalDivisionNeighborhood(thpThirdPartyDto.getIdPoliticalDivisionNeighborhood());
        thpThirdPartyEntity.setIdThirdPartyType(thpThirdPartyDto.getIdThirdPartyType());
        thpThirdPartyEntity.setIdListLegalNature(thpThirdPartyDto.getIdListLegalNature());
        thpThirdPartyEntity.setIdListTypeIdentification(thpThirdPartyDto.getIdListTypeIdentification());
        thpThirdPartyEntity.setIdListGender(thpThirdPartyDto.getIdListGender());
        thpThirdPartyEntity.setPoliticalDivisionCity(thpThirdPartyDto.getPoliticalDivisionCity());
        thpThirdPartyEntity.setPoliticalDivisionNeighborhood(thpThirdPartyDto.getPoliticalDivisionNeighborhood());
        thpThirdPartyEntity.setThirdPartyType(thpThirdPartyDto.getThirdPartyType());
        thpThirdPartyEntity.setListLegalNature(thpThirdPartyDto.getListLegalNature());
        thpThirdPartyEntity.setListTypeIdentification(thpThirdPartyDto.getListTypeIdentification());
        thpThirdPartyEntity.setListGender(thpThirdPartyDto.getListGender());
        return thpThirdPartyEntity;
    }
}
