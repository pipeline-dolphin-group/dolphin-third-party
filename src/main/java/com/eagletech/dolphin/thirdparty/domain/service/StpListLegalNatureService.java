package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;

import java.util.List;
import java.util.Optional;

public interface StpListLegalNatureService {

    List<StpListLegalNatureEntity> findAll();

    Optional<StpListLegalNatureEntity> findById(Long id);

    StpListLegalNatureEntity save(StpListLegalNatureEntity stpListLegalNatureEntity);
}
