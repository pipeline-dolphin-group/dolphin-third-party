package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;

import java.util.List;
import java.util.Optional;

public interface StpListPoliticalDivisionGroupService {

    List<StpListPoliticalDivisionGroupEntity> findAll();

    Optional<StpListPoliticalDivisionGroupEntity> findById(Long id);

    StpListPoliticalDivisionGroupEntity save(StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity);
}
