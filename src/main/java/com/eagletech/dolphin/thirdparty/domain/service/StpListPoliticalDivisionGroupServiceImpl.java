package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionGroupDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionGroupService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListPoliticalDivisionGroupServiceImpl implements ListPoliticalDivisionGroupService {

    private final StpListPoliticalDivisionGroupService service;

    @Autowired
    public StpListPoliticalDivisionGroupServiceImpl(StpListPoliticalDivisionGroupService service) {
        this.service = service;
    }

    public List<StpListPoliticalDivisionGroupEntity> getStpListPoliticalDivisionGroups() {
        return service.findAll();
    }

    public StpListPoliticalDivisionGroupDto saveStpListPoliticalDivisionGroup(StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto) {
        stpListPoliticalDivisionGroupDto.setDatetimeLog(new Date());
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = StpListPoliticalDivisionGroupBuilder.convertToEntity(stpListPoliticalDivisionGroupDto);
        return StpListPoliticalDivisionGroupBuilder.convertToDomain(service.save(stpListPoliticalDivisionGroupEntity));
    }

    @Override
    public StpListPoliticalDivisionGroupDto updateStpListPoliticalDivisionGroup(StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto) {
        StpListPoliticalDivisionGroupEntity existingStpListPoliticalDivisionGroupEntity = service.findById(stpListPoliticalDivisionGroupDto.getIdListPoliticalDivisionGroup()).orElse(null);
        StpListPoliticalDivisionGroupDto existingStpListPoliticalDivisionGroupDto = StpListPoliticalDivisionGroupBuilder.convertToDomain(existingStpListPoliticalDivisionGroupEntity);
        BeanUtils.copyProperties(stpListPoliticalDivisionGroupDto, existingStpListPoliticalDivisionGroupDto);
        existingStpListPoliticalDivisionGroupDto.setIdListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto.getIdListPoliticalDivisionGroup());
        existingStpListPoliticalDivisionGroupDto.setDatetimeLog(new Date());
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroup = StpListPoliticalDivisionGroupBuilder.convertToEntity(existingStpListPoliticalDivisionGroupDto);
        return StpListPoliticalDivisionGroupBuilder.convertToDomain(service.save(stpListPoliticalDivisionGroup));
    }

}
