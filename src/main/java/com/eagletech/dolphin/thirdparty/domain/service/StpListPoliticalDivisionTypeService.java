package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StpListPoliticalDivisionTypeService {

    List<StpListPoliticalDivisionTypeEntity> findAll();

    Page<StpListPoliticalDivisionTypeEntity> findAllWithPagination(int offset, int pageSize);

    Optional<StpListPoliticalDivisionTypeEntity> findById(Long id);

    StpListPoliticalDivisionTypeEntity save(StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity);
}
