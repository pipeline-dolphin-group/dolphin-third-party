package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;
import com.eagletech.dolphin.thirdparty.application.service.ListLegalNatureService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListLegalNatureServiceImpl implements ListLegalNatureService {

    private final StpListLegalNatureService service;

    @Autowired
    public StpListLegalNatureServiceImpl(StpListLegalNatureService service) {
        this.service = service;
    }

    public List<StpListLegalNatureEntity> getListLegalNatures() {
        return service.findAll();
    }

    public StpListLegalNatureDto saveStpListLegalNature(StpListLegalNatureDto stpListLegalNatureDto) {
        stpListLegalNatureDto.setDatetimeLog(new Date());
        StpListLegalNatureEntity stpListLegalNatureEntity = StpListLegalNatureBuilder.convertToEntity(stpListLegalNatureDto);
        return StpListLegalNatureBuilder.convertToDomain(service.save(stpListLegalNatureEntity));
    }

    public StpListLegalNatureDto updateStpListLegalNature(StpListLegalNatureDto stpListLegalNatureDto) {
        StpListLegalNatureEntity existingStpListLegalNatureEntity = service.findById(stpListLegalNatureDto.getIdListLegalNature()).orElse(null);
        StpListLegalNatureDto existingStpListLegalNatureDto = StpListLegalNatureBuilder.convertToDomain(existingStpListLegalNatureEntity);
        BeanUtils.copyProperties(stpListLegalNatureDto, existingStpListLegalNatureDto);
        existingStpListLegalNatureDto.setIdListLegalNature(stpListLegalNatureDto.getIdListLegalNature());
        existingStpListLegalNatureDto.setDatetimeLog(new Date());
        StpListLegalNatureEntity stpListLegalNature = StpListLegalNatureBuilder.convertToEntity(existingStpListLegalNatureDto);
        return StpListLegalNatureBuilder.convertToDomain(service.save(stpListLegalNature));
    }
}
