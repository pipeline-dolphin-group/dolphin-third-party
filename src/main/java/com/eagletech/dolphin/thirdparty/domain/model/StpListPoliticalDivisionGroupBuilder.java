package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionGroupDto;

public class StpListPoliticalDivisionGroupBuilder {

    private StpListPoliticalDivisionGroupBuilder() {}

    /**
     *
     * Description: This method convert a Political Division Group entity to Political Division Group dto.
     *
     * @param stpListPoliticalDivisionGroupEntity
     * @return
     */
    public static StpListPoliticalDivisionGroupDto convertToDomain(StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity) {
        StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto = null;
        if (stpListPoliticalDivisionGroupEntity != null) {
            stpListPoliticalDivisionGroupDto = new StpListPoliticalDivisionGroupDto(
                    stpListPoliticalDivisionGroupEntity.getIdListPoliticalDivisionGroup(),
                    stpListPoliticalDivisionGroupEntity.getCode(),
                    stpListPoliticalDivisionGroupEntity.getName(),
                    stpListPoliticalDivisionGroupEntity.getState(),
                    stpListPoliticalDivisionGroupEntity.getIdUserLog(),
                    stpListPoliticalDivisionGroupEntity.getDatetimeLog()
            );
        }
        return stpListPoliticalDivisionGroupDto;
    }

    /**
     *
     * Description: This method convert a Political Division Group dto to Political Division Group entity.
     *
     * @param stpListPoliticalDivisionGroupDto
     * @return
     */
    public static StpListPoliticalDivisionGroupEntity convertToEntity(StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto) {
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new StpListPoliticalDivisionGroupEntity();
        stpListPoliticalDivisionGroupEntity.setIdListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto.getIdListPoliticalDivisionGroup());
        stpListPoliticalDivisionGroupEntity.setCode(stpListPoliticalDivisionGroupDto.getCode());
        stpListPoliticalDivisionGroupEntity.setName(stpListPoliticalDivisionGroupDto.getName());
        stpListPoliticalDivisionGroupEntity.setState(stpListPoliticalDivisionGroupDto.getState());
        stpListPoliticalDivisionGroupEntity.setIdUserLog(stpListPoliticalDivisionGroupDto.getIdUserLog());
        stpListPoliticalDivisionGroupEntity.setDatetimeLog(stpListPoliticalDivisionGroupDto.getDatetimeLog());
        return stpListPoliticalDivisionGroupEntity;
    }
}
