package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListTypeIdentificationDto;
import com.eagletech.dolphin.thirdparty.application.service.ListTypeIdentificationService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListTypeIdentificationServiceImpl implements ListTypeIdentificationService {

    @Autowired
    private StpListTypeIdentificationService repositoryService;

    /**
     *
     * Description: This method list all Type Identification record to the database.
     *
     * @return List<StpListTypeIdentificationEntity>
     */
    @Override
    public List<StpListTypeIdentificationEntity> getListTypeIdentifications() {
        return repositoryService.findAll();
    }

    /**
     *
     * Description: This method list all Type Identification record to the database with pagination.
     *
     * @param offset
     * @param pageSize
     * @return
     */
    @Override
    public Page<StpListTypeIdentificationEntity> findAllWithPagination(int offset, int pageSize) {
        return repositoryService.findAllWithPagination(offset, pageSize);
    }

    /**
     *
     * Description: This method get one Type Identification record to the database with the ID.
     *
     * @param id
     * @return
     */
    @Override
    public StpListTypeIdentificationEntity getListTypeIdentificationById(Long id) {
        return repositoryService.findById(id).orElse(null);
    }

    /**
     *
     * Description: This method save the Type Identification into the persistence layer.
     *
     * @param listTypeIdentificationDto
     * @return
     */
    @Override
    public StpListTypeIdentificationDto saveStpListTypeIdentification(StpListTypeIdentificationDto listTypeIdentificationDto) {
        listTypeIdentificationDto.setDatetimeLog(new Date());
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = StpListTypeIdentificationBuilder.convertToEntity(listTypeIdentificationDto);
        return StpListTypeIdentificationBuilder.convertToDomain(repositoryService.save(stpListTypeIdentificationEntity));
    }

    /**
     *
     * Description: This method update the Type Identification into the persistence layer.
     *
     * @param listTypeIdentificationDto
     * @return
     */
    @Override
    public StpListTypeIdentificationDto updateStpListTypeIdentification(StpListTypeIdentificationDto listTypeIdentificationDto) {
        StpListTypeIdentificationEntity existingStpListTypeIdentificationEntity = repositoryService.findById(listTypeIdentificationDto.getIdListTypeIdentification()).orElse(null);
        StpListTypeIdentificationDto existingStpListTypeIdentificationDto = StpListTypeIdentificationBuilder.convertToDomain(existingStpListTypeIdentificationEntity);
        BeanUtils.copyProperties(listTypeIdentificationDto, existingStpListTypeIdentificationDto);
        existingStpListTypeIdentificationDto.setIdListTypeIdentification(listTypeIdentificationDto.getIdListTypeIdentification());
        existingStpListTypeIdentificationDto.setDatetimeLog(new Date());
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = StpListTypeIdentificationBuilder.convertToEntity(existingStpListTypeIdentificationDto);
        return StpListTypeIdentificationBuilder.convertToDomain(repositoryService.save(stpListTypeIdentificationEntity));
    }
}
