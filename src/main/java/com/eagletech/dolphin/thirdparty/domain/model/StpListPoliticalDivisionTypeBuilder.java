package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionTypeDto;

public class StpListPoliticalDivisionTypeBuilder {

    private StpListPoliticalDivisionTypeBuilder() {}

    /**
     *
     * Description: This method convert a Political Division Type entity to Political Division Type dto.
     *
     * @param stpListPoliticalDivisionTypeEntity
     * @return
     */
    public static StpListPoliticalDivisionTypeDto convertToDomain(StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity) {
        StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto = null;
        if (stpListPoliticalDivisionTypeEntity != null) {
            stpListPoliticalDivisionTypeDto = new StpListPoliticalDivisionTypeDto(
                    stpListPoliticalDivisionTypeEntity.getIdListPoliticalDivisionType(),
                    stpListPoliticalDivisionTypeEntity.getCode(),
                    stpListPoliticalDivisionTypeEntity.getName(),
                    stpListPoliticalDivisionTypeEntity.getState(),
                    stpListPoliticalDivisionTypeEntity.getIdUserLog(),
                    stpListPoliticalDivisionTypeEntity.getDatetimeLog()
            );
        }
        return stpListPoliticalDivisionTypeDto;
    }

    /**
     *
     * Description: This method convert a Political Division Type dto to Political Division Type entity.
     *
     * @param stpListPoliticalDivisionTypeDto
     * @return
     */
    public static StpListPoliticalDivisionTypeEntity convertToEntity(StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto) {
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new StpListPoliticalDivisionTypeEntity();
        stpListPoliticalDivisionTypeEntity.setIdListPoliticalDivisionType(stpListPoliticalDivisionTypeDto.getIdListPoliticalDivisionType());
        stpListPoliticalDivisionTypeEntity.setCode(stpListPoliticalDivisionTypeDto.getCode());
        stpListPoliticalDivisionTypeEntity.setName(stpListPoliticalDivisionTypeDto.getName());
        stpListPoliticalDivisionTypeEntity.setState(stpListPoliticalDivisionTypeDto.getState());
        stpListPoliticalDivisionTypeEntity.setIdUserLog(stpListPoliticalDivisionTypeDto.getIdUserLog());
        stpListPoliticalDivisionTypeEntity.setDatetimeLog(stpListPoliticalDivisionTypeDto.getDatetimeLog());
        return stpListPoliticalDivisionTypeEntity;
    }
}
