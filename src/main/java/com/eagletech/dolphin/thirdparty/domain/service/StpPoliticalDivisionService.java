package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StpPoliticalDivisionService {

    List<StpPoliticalDivisionEntity> findAll();

    Page<StpPoliticalDivisionEntity> findAllWithPagination(int offset, int pageSize);

    Optional<StpPoliticalDivisionEntity> findById(Long id);

    StpPoliticalDivisionEntity save(StpPoliticalDivisionEntity stpPoliticalDivisionEntity);

    List<StpPoliticalDivisionEntity> saveAll(List<StpPoliticalDivisionEntity> stpPoliticalDivisionEntities);
}
