package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpPoliticalDivisionDto;

public class StpPoliticalDivisionBuilder {

    private StpPoliticalDivisionBuilder() {
    }

    /**
     * Description: This method convert a political division entity to political division dto.
     *
     * @param stpPoliticalDivisionEntity
     * @return
     */
    public static StpPoliticalDivisionDto convertToDomain(StpPoliticalDivisionEntity stpPoliticalDivisionEntity) {
        StpPoliticalDivisionDto stpPoliticalDivisionDto = null;
        if (stpPoliticalDivisionEntity != null) {
            stpPoliticalDivisionDto = new StpPoliticalDivisionDto(
                    stpPoliticalDivisionEntity.getIdPoliticalDivision(),
                    stpPoliticalDivisionEntity.getIdPoliticalDivisionFather(),
                    stpPoliticalDivisionEntity.getIdListPoliticalDivisionType(),
                    stpPoliticalDivisionEntity.getIdListPoliticalDivisionGroup(),
                    stpPoliticalDivisionEntity.getCode(),
                    stpPoliticalDivisionEntity.getName(),
                    stpPoliticalDivisionEntity.isState(),
                    stpPoliticalDivisionEntity.getIdUserLog(),
                    stpPoliticalDivisionEntity.getDatetimeLog(),
                    stpPoliticalDivisionEntity.getPoliticalDivisionFather(),
                    stpPoliticalDivisionEntity.getPoliticalDivisionType(),
                    stpPoliticalDivisionEntity.getListPoliticalDivisionGroup()
            );
        }
        return stpPoliticalDivisionDto;
    }

    /**
     * Description: This method convert a political division dto to political division entity.
     *
     * @param stpPoliticalDivisionDto
     * @return
     */
    public static StpPoliticalDivisionEntity convertToEntity(StpPoliticalDivisionDto stpPoliticalDivisionDto) {
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new StpPoliticalDivisionEntity();
        stpPoliticalDivisionEntity.setIdPoliticalDivision(stpPoliticalDivisionDto.getIdPoliticalDivision());
        stpPoliticalDivisionEntity.setIdPoliticalDivisionFather(stpPoliticalDivisionDto.getIdPoliticalDivisionFather());
        stpPoliticalDivisionEntity.setIdListPoliticalDivisionType(stpPoliticalDivisionDto.getIdListPoliticalDivisionType());
        stpPoliticalDivisionEntity.setIdListPoliticalDivisionGroup(stpPoliticalDivisionDto.getIdListPoliticalDivisionGroup());
        stpPoliticalDivisionEntity.setCode(stpPoliticalDivisionDto.getCode());
        stpPoliticalDivisionEntity.setName(stpPoliticalDivisionDto.getName());
        stpPoliticalDivisionEntity.setState(stpPoliticalDivisionDto.isState());
        stpPoliticalDivisionEntity.setIdUserLog(stpPoliticalDivisionDto.getIdUserLog());
        stpPoliticalDivisionEntity.setDatetimeLog(stpPoliticalDivisionDto.getDatetimeLog());
        stpPoliticalDivisionEntity.setPoliticalDivisionFather(stpPoliticalDivisionDto.getPoliticalDivisionFather());
        stpPoliticalDivisionEntity.setPoliticalDivisionType(stpPoliticalDivisionDto.getPoliticalDivisionType());
        stpPoliticalDivisionEntity.setListPoliticalDivisionGroup(stpPoliticalDivisionDto.getListPoliticalDivisionGroup());
        return stpPoliticalDivisionEntity;
    }
}
