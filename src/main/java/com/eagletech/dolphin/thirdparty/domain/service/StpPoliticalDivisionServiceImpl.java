package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpPoliticalDivisionDto;
import com.eagletech.dolphin.thirdparty.application.service.PoliticalDivisionService;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StpPoliticalDivisionServiceImpl implements PoliticalDivisionService {

    @Autowired
    private StpPoliticalDivisionService repositoryService;

    /**
     * Description: This method list all political division record to the database.
     *
     * @return List<StpPoliticalDivisionEntity>
     */
    @Override
    public List<StpPoliticalDivisionEntity> getPoliticalDivision() {
        return repositoryService.findAll();
    }

    /**
     * Description: This method list all political division record to the database with pagination.
     *
     * @return List<StpPoliticalDivisionEntity>
     */
    @Override
    public Page<StpPoliticalDivisionEntity> findAllWithPagination(int offset, int pageSize) {
        return repositoryService.findAllWithPagination(offset, pageSize);
    }

    /**
     * Description: This method get one political division record to the database with the ID.
     *
     * @param id This param is to validate if the political division exists with this ID.
     * @return StpPoliticalDivisionEntity
     */
    @Override
    public StpPoliticalDivisionEntity getPoliticalDivisionById(Long id) {
        return repositoryService.findById(id).orElse(null);
    }

    /**
     * Description: This method save the political division into the persistence layer.
     *
     * @param stpPoliticalDivisionDto this param is used to save the political division.
     * @return StpPoliticalDivisionDto
     */
    @Override
    public StpPoliticalDivisionDto savePoliticalDivision(StpPoliticalDivisionDto stpPoliticalDivisionDto) {
        stpPoliticalDivisionDto.setDatetimeLog(new Date());
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = StpPoliticalDivisionBuilder.convertToEntity(stpPoliticalDivisionDto);
        return StpPoliticalDivisionBuilder.convertToDomain(repositoryService.save(stpPoliticalDivisionEntity));
    }

    /**
     * Description: This method create a list of political division into the persistence layer.
     *
     * @param stpPoliticalDivisionsDto this param is used to saved political division
     * @return List<StpPoliticalDivisionDto>
     */
    @Override
    public List<StpPoliticalDivisionDto> savePoliticalDivisions(List<StpPoliticalDivisionDto> stpPoliticalDivisionsDto) {
        List<StpPoliticalDivisionDto> politicalDivisionsDto = new ArrayList<>();
        List<StpPoliticalDivisionEntity> politicalDivisionEntity = new ArrayList<>();
        stpPoliticalDivisionsDto.forEach(stpPoliticalDivisionDto -> {
            stpPoliticalDivisionDto.setDatetimeLog(new Date());
            politicalDivisionEntity.add(StpPoliticalDivisionBuilder.convertToEntity(stpPoliticalDivisionDto));
        });
        repositoryService.saveAll(politicalDivisionEntity)
                .forEach(stpPoliticalDivisionEntity -> politicalDivisionsDto.add(StpPoliticalDivisionBuilder
                        .convertToDomain(stpPoliticalDivisionEntity)));
        return politicalDivisionsDto;
    }

    /**
     * Description: This method update the political division into the persistence layer.
     *
     *
     * @param stpPoliticalDivisionDto this param is used to update political division.
     * @return StpPoliticalDivisionEntity
     */
    @Override
    public StpPoliticalDivisionDto updatePoliticalDivision(StpPoliticalDivisionDto stpPoliticalDivisionDto) {
        StpPoliticalDivisionEntity existingPoliticalDivisionEntity = repositoryService.findById(stpPoliticalDivisionDto.getIdPoliticalDivision()).orElse(null);
        StpPoliticalDivisionDto existingPoliticalDivisionDto = StpPoliticalDivisionBuilder.convertToDomain(existingPoliticalDivisionEntity);
        BeanUtils.copyProperties(stpPoliticalDivisionDto, existingPoliticalDivisionDto);
        existingPoliticalDivisionDto.setIdPoliticalDivision(stpPoliticalDivisionDto.getIdPoliticalDivision());
        existingPoliticalDivisionDto.setDatetimeLog(new Date());
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = StpPoliticalDivisionBuilder.convertToEntity(existingPoliticalDivisionDto);
        return StpPoliticalDivisionBuilder.convertToDomain(repositoryService.save(stpPoliticalDivisionEntity));
    }
}
