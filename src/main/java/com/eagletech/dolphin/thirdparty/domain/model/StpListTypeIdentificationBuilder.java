package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListTypeIdentificationDto;

public class StpListTypeIdentificationBuilder {

    private StpListTypeIdentificationBuilder() {
    }

    /**
     *
     * Description: This method convert a Type Identification entity to Type Identification dto.
     *
     * @param stpListTypeIdentificationEntity
     * @return
     */
    public static StpListTypeIdentificationDto convertToDomain(StpListTypeIdentificationEntity stpListTypeIdentificationEntity) {
        StpListTypeIdentificationDto stpListTypeIdentificationDto = null;
        if (stpListTypeIdentificationEntity != null) {
            stpListTypeIdentificationDto = new StpListTypeIdentificationDto(
                    stpListTypeIdentificationEntity.getIdListTypeIdentification(),
                    stpListTypeIdentificationEntity.getCode(),
                    stpListTypeIdentificationEntity.getName(),
                    stpListTypeIdentificationEntity.getState(),
                    stpListTypeIdentificationEntity.getIdUserLog(),
                    stpListTypeIdentificationEntity.getDatetimeLog()
            );
        }
        return stpListTypeIdentificationDto;
    }

    /**
     *
     * Description: This method convert a Type Identification dto to Type Identification entity.
     *
     * @param stpListTypeIdentificationDto
     * @return
     */
    public static StpListTypeIdentificationEntity convertToEntity(StpListTypeIdentificationDto stpListTypeIdentificationDto) {
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new StpListTypeIdentificationEntity();
        stpListTypeIdentificationEntity.setIdListTypeIdentification(stpListTypeIdentificationDto.getIdListTypeIdentification());
        stpListTypeIdentificationEntity.setCode(stpListTypeIdentificationDto.getCode());
        stpListTypeIdentificationEntity.setName(stpListTypeIdentificationDto.getName());
        stpListTypeIdentificationEntity.setState(stpListTypeIdentificationDto.getState());
        stpListTypeIdentificationEntity.setIdUserLog(stpListTypeIdentificationDto.getIdUserLog());
        stpListTypeIdentificationEntity.setDatetimeLog(stpListTypeIdentificationDto.getDatetimeLog());
        return stpListTypeIdentificationEntity;
    }
}
