package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ThpThirdPartyService {

    List<ThpThirdPartyEntity> findAll();

    Page<ThpThirdPartyEntity> findAllWithPagination(int offset, int pageSize);

    List<ThpThirdPartyEntity> findByNumberIdentificationOrSurname(String param);

    Optional<ThpThirdPartyEntity> findById(Long id);

    ThpThirdPartyEntity save(ThpThirdPartyEntity thirdParty);

    List<ThpThirdPartyEntity> saveAll(List<ThpThirdPartyEntity> thirdParties);
}
