package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.ThpThirdPartyDto;
import com.eagletech.dolphin.thirdparty.application.service.ThirdPartyService;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ThpThirdPartyServiceImpl implements ThirdPartyService {

    @Autowired
    private ThpThirdPartyService repositoryService;

    /**
     * Description: This method list all third party record to the database.
     *
     * @return List<ThpThirdPartyEntity>
     */
    @Override
    public List<ThpThirdPartyEntity> getThirdParty() {
        return repositoryService.findAll();
    }

    /**
     * Description: This method list all third party record to the database with pagination.
     *
     * @return List<ThpThirdPartyEntity>
     */
    @Override
    public Page<ThpThirdPartyEntity> findAllWithPagination(int offset, int pageSize) {
        return repositoryService.findAllWithPagination(offset, pageSize);
    }

    /**
     * Description: This method get all the third party with the same param finding on the database.
     *
     * @param param This param is to find records with the same param.
     * @return List<ThpThirdPartyEntity>
     */
    @Override
    public List<ThpThirdPartyEntity> findByNameOrIdentification(String param) {
        StringBuilder parameter = new StringBuilder();
        if (param.length() > 2) {
            parameter.append(param);
        }
        return repositoryService.findByNumberIdentificationOrSurname(parameter.toString());
    }

    /**
     * Description: This method get one third party record to the database with the ID.
     *
     * @param id This param is to validate if the third party exists with this ID.
     * @return ThpThirdPartyEntity
     */
    @Override
    public ThpThirdPartyEntity getThirdPartyById(Long id) {
        return repositoryService.findById(id).orElse(null);
    }

    /**
     * Description: This method save the third party into the persistence layer.
     *
     * @param thirdPartyDto this param is used to save the third party.
     * @return ThpThirdPartyEntity
     */
    @Override
    public ThpThirdPartyDto saveThirdParty(ThpThirdPartyDto thirdPartyDto) {
        thirdPartyDto.setDatetimeLog(new Date());
        ThpThirdPartyEntity thpThirdPartyEntity = ThpThirdPartyBuilder.convertToEntity(thirdPartyDto);
        return ThpThirdPartyBuilder.convertToDomain(repositoryService.save(thpThirdPartyEntity));
    }

    /**
     * Description: This method create a list of third parties into the persistence layer.
     *
     * @param thirdPartiesDto this param is used to saved third parties
     * @return List<ThpThirdPartyEntity>
     */
    @Override
    public List<ThpThirdPartyDto> saveThirdParties(List<ThpThirdPartyDto> thirdPartiesDto) {
        List<ThpThirdPartyEntity> thirdPartyEntities = new ArrayList<>();
        List<ThpThirdPartyDto> thpThirdPartyDto = new ArrayList<>();
        thirdPartiesDto.forEach(thirdPartyDto -> {
            thirdPartyDto.setDatetimeLog(new Date());
            ThpThirdPartyEntity thirdPartyEntity = ThpThirdPartyBuilder.convertToEntity(thirdPartyDto);
            thirdPartyEntities.add(thirdPartyEntity);
        });
        repositoryService.saveAll(thirdPartyEntities).forEach(thpThirdPartyEntity -> {
            ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
            thpThirdPartyDto.add(thirdPartyDto);
        });
        return thpThirdPartyDto;
    }

    /**
     * Description: This method update the third party into the persistence layer.
     *
     *
     * @param thirdPartyDto this param is used to update third party.
     * @return ThpThirdPartyEntity
     */
    @Override
    public ThpThirdPartyDto updateThirdParty(ThpThirdPartyDto thirdPartyDto) {
        ThpThirdPartyEntity existingThirdParty = repositoryService.findById(thirdPartyDto.getIdThirdParty()).orElse(null);
        ThpThirdPartyDto existingThirdPartyDto = ThpThirdPartyBuilder.convertToDomain(existingThirdParty);
        BeanUtils.copyProperties(thirdPartyDto, existingThirdPartyDto);
        existingThirdPartyDto.setIdThirdParty(thirdPartyDto.getIdThirdParty());
        existingThirdPartyDto.setDatetimeLog(new Date());
        ThpThirdPartyEntity thpThirdParty = ThpThirdPartyBuilder.convertToEntity(existingThirdPartyDto);
        return ThpThirdPartyBuilder.convertToDomain(repositoryService.save(thpThirdParty));
    }

}
