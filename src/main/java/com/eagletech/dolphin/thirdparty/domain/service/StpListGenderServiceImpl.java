package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListGenderDto;
import com.eagletech.dolphin.thirdparty.application.service.ListGenderService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListGenderServiceImpl implements ListGenderService {

    private StpListGenderService service;

    @Autowired
    public StpListGenderServiceImpl(StpListGenderService service) {
        this.service = service;
    }

    public List<StpListGenderEntity> getListGenders() {
        return service.findAll();
    }

    public StpListGenderDto saveStpListGender(StpListGenderDto stpListGenderDto) {
        stpListGenderDto.setDatetimeLog(new Date());
        StpListGenderEntity stpListGenderEntity = StpListGenderBuilder.convertToEntity(stpListGenderDto);
        return StpListGenderBuilder.convertToDomain(service.save(stpListGenderEntity));
    }

    public StpListGenderDto updateStpListGender(StpListGenderDto stpListGenderDto) {
        StpListGenderEntity existingStpListGenderEntity = service.findById(stpListGenderDto.getIdListGender()).orElse(null);
        StpListGenderDto existingStpListGenderDto = StpListGenderBuilder.convertToDomain(existingStpListGenderEntity);
        BeanUtils.copyProperties(stpListGenderDto, existingStpListGenderDto);
        existingStpListGenderDto.setIdListGender(stpListGenderDto.getIdListGender());
        existingStpListGenderDto.setDatetimeLog(new Date());
        StpListGenderEntity stpListLegalNature = StpListGenderBuilder.convertToEntity(existingStpListGenderDto);
        return StpListGenderBuilder.convertToDomain(service.save(stpListLegalNature));
    }
}
