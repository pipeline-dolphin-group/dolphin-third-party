package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;

public class StpListLegalNatureBuilder {

    private StpListLegalNatureBuilder() {}

    /**
     *
     * Description: This method convert a Legal Nature entity to Legal Nature dto.
     *
     * @param stpListLegalNatureEntity
     * @return
     */
    public static StpListLegalNatureDto convertToDomain(StpListLegalNatureEntity stpListLegalNatureEntity) {
        StpListLegalNatureDto stpListLegalNatureDto = null;
        if (stpListLegalNatureEntity != null) {
            stpListLegalNatureDto = new StpListLegalNatureDto(
                    stpListLegalNatureEntity.getIdListLegalNature(),
                    stpListLegalNatureEntity.getCode(),
                    stpListLegalNatureEntity.getName(),
                    stpListLegalNatureEntity.getState(),
                    stpListLegalNatureEntity.getIdUserLog(),
                    stpListLegalNatureEntity.getDatetimeLog()
            );
        }
        return stpListLegalNatureDto;
    }

    /**
     *
     * Description: This method convert a Legal Nature dto to Political Legal Nature entity.
     *
     * @param listLegalNatureDto
     * @return
     */
    public static StpListLegalNatureEntity convertToEntity(StpListLegalNatureDto listLegalNatureDto) {
        StpListLegalNatureEntity stpListLegalNatureEntity = new StpListLegalNatureEntity();
        stpListLegalNatureEntity.setIdListLegalNature(listLegalNatureDto.getIdListLegalNature());
        stpListLegalNatureEntity.setCode(listLegalNatureDto.getCode());
        stpListLegalNatureEntity.setName(listLegalNatureDto.getName());
        stpListLegalNatureEntity.setState(listLegalNatureDto.getState());
        stpListLegalNatureEntity.setIdUserLog(listLegalNatureDto.getIdUserLog());
        stpListLegalNatureEntity.setDatetimeLog(listLegalNatureDto.getDatetimeLog());
        return stpListLegalNatureEntity;
    }
}
