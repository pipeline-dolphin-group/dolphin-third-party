package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StpListThirdPartyTypeService {

    List<StpListThirdPartyTypeEntity> findAll();

    Page<StpListThirdPartyTypeEntity> findAllWithPagination(int offset, int pageSize);

    Optional<StpListThirdPartyTypeEntity> findById(Long id);

    StpListThirdPartyTypeEntity save(StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity);
}
