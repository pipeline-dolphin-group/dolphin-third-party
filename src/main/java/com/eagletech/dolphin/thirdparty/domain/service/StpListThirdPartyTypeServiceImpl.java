package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListThirdPartyTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListThirdPartyTypeService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListThirdPartyTypeServiceImpl implements ListThirdPartyTypeService {

    @Autowired
    private StpListThirdPartyTypeService repositoryService;

    @Override
    public List<StpListThirdPartyTypeEntity> getListThirdPartyType() {
        return repositoryService.findAll();
    }

    @Override
    public Page<StpListThirdPartyTypeEntity> findAllWithPagination(int offset, int pageSize) {
        return repositoryService.findAllWithPagination(offset, pageSize);
    }

    @Override
    public StpListThirdPartyTypeDto saveStpListThirdPartyType(StpListThirdPartyTypeDto stpListThirdPartyTypeDto) {
        stpListThirdPartyTypeDto.setDatetimeLog(new Date());
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = StpListThirdPartyTypeBuilder.convertToEntity(stpListThirdPartyTypeDto);
        return StpListThirdPartyTypeBuilder.convertToDomain(repositoryService.save(stpListThirdPartyTypeEntity));
    }

    @Override
    public StpListThirdPartyTypeDto
    updateStpListThirdPartyType(StpListThirdPartyTypeDto stpListThirdPartyTypeDto) {
        StpListThirdPartyTypeEntity existingStpListThirdPartyType = repositoryService.findById(stpListThirdPartyTypeDto.getIdListThirdPartyType()).orElse(null);
        StpListThirdPartyTypeDto existingStpListThirdPartyDto = StpListThirdPartyTypeBuilder.convertToDomain(existingStpListThirdPartyType);
        BeanUtils.copyProperties(stpListThirdPartyTypeDto, existingStpListThirdPartyDto);
        existingStpListThirdPartyDto.setIdListThirdPartyType(stpListThirdPartyTypeDto.getIdListThirdPartyType());
        existingStpListThirdPartyDto.setDatetimeLog(new Date());
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = StpListThirdPartyTypeBuilder.convertToEntity(existingStpListThirdPartyDto);
        return StpListThirdPartyTypeBuilder.convertToDomain(repositoryService.save(stpListThirdPartyTypeEntity));
    }
}
