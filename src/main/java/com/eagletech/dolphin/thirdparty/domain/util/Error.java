package com.eagletech.dolphin.thirdparty.domain.util;

import lombok.Data;

@Data
public class Error {
    private String field;
    private String message;

    public Error(String field, String message){
        this.field = field;
        this.message = message;
    }
}
