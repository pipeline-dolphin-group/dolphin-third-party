package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StpListTypeIdentificationService {

    List<StpListTypeIdentificationEntity> findAll();

    Page<StpListTypeIdentificationEntity> findAllWithPagination(int offset, int pageSize);

    Optional<StpListTypeIdentificationEntity> findById(Long id);

    StpListTypeIdentificationEntity save(StpListTypeIdentificationEntity stpPoliticalDivisionEntity);

    List<StpListTypeIdentificationEntity> saveAll(List<StpListTypeIdentificationEntity> stpPoliticalDivisionEntities);
}
