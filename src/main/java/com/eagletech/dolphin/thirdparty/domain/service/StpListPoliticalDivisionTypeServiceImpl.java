package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionTypeService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StpListPoliticalDivisionTypeServiceImpl implements ListPoliticalDivisionTypeService {

    private final StpListPoliticalDivisionTypeService repository;

    @Autowired
    public StpListPoliticalDivisionTypeServiceImpl(StpListPoliticalDivisionTypeService repository) {
        this.repository = repository;
    }

    @Override
    public List<StpListPoliticalDivisionTypeEntity> getStpListPoliticalDivisionTypes() {
        return repository.findAll();
    }

    @Override
    public StpListPoliticalDivisionTypeDto saveStpListPoliticalDivisionType(StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto) {
        stpListPoliticalDivisionTypeDto.setDatetimeLog(new Date());
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = StpListPoliticalDivisionTypeBuilder.convertToEntity(stpListPoliticalDivisionTypeDto);
        return StpListPoliticalDivisionTypeBuilder.convertToDomain(repository.save(stpListPoliticalDivisionTypeEntity));
    }

    @Override
    public StpListPoliticalDivisionTypeDto updateStpListPoliticalDivisionType(StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto) {
        StpListPoliticalDivisionTypeEntity existingStpListPoliticalDivisionTypeEntity = repository.findById(stpListPoliticalDivisionTypeDto.getIdListPoliticalDivisionType()).orElse(null);
        StpListPoliticalDivisionTypeDto existingStpListPoliticalDivisionTypeDto = StpListPoliticalDivisionTypeBuilder.convertToDomain(existingStpListPoliticalDivisionTypeEntity);
        BeanUtils.copyProperties(stpListPoliticalDivisionTypeDto, existingStpListPoliticalDivisionTypeDto);
        existingStpListPoliticalDivisionTypeDto.setIdListPoliticalDivisionType(stpListPoliticalDivisionTypeDto.getIdListPoliticalDivisionType());
        existingStpListPoliticalDivisionTypeDto.setDatetimeLog(new Date());
        StpListPoliticalDivisionTypeEntity listPoliticalDivisionType = StpListPoliticalDivisionTypeBuilder.convertToEntity(existingStpListPoliticalDivisionTypeDto);
        return StpListPoliticalDivisionTypeBuilder.convertToDomain(repository.save(listPoliticalDivisionType));
    }

}
