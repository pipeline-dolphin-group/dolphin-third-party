package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;

import java.util.List;
import java.util.Optional;

public interface StpListGenderService {

    List<StpListGenderEntity> findAll();

    Optional<StpListGenderEntity> findById(Long id);

    StpListGenderEntity save(StpListGenderEntity stpListGenderEntity);
}
