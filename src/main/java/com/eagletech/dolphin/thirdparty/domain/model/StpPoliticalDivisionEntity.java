package com.eagletech.dolphin.thirdparty.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stp_political_division")
public class StpPoliticalDivisionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_political_division")
    private long idPoliticalDivision;
    @Column(name = "id_political_division_father")
    private Long idPoliticalDivisionFather;
    @Column(name = "id_list_political_division_type")
    private Long idListPoliticalDivisionType;
    @Column(name = "id_list_political_division_group")
    private Long idListPoliticalDivisionGroup;
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "state")
    private boolean state;
    @Column(name = "id_user_log")
    private Long idUserLog;
    @Column(name = "datetime_log")
    private Date datetimeLog;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_political_division_father", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpPoliticalDivisionEntity politicalDivisionFather;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_list_political_division_type", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListPoliticalDivisionTypeEntity politicalDivisionType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_list_political_division_group", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private StpListPoliticalDivisionGroupEntity listPoliticalDivisionGroup;
}
