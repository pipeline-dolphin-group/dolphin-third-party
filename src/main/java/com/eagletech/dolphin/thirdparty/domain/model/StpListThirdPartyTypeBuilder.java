package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListThirdPartyTypeDto;

public class StpListThirdPartyTypeBuilder {

    private StpListThirdPartyTypeBuilder() {
    }

    /**
     *
     * Description: This method convert a third party type entity to third party type dto.
     *
     * @param stpListThirdPartyTypeEntity
     * @return
     */
    public static StpListThirdPartyTypeDto convertToDomain(StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity) {
        StpListThirdPartyTypeDto stpListThirdPartyTypeDto = null;
        if (stpListThirdPartyTypeEntity != null) {
            stpListThirdPartyTypeDto = new StpListThirdPartyTypeDto(
                    stpListThirdPartyTypeEntity.getIdListThirdPartyType(),
                    stpListThirdPartyTypeEntity.getCode(),
                    stpListThirdPartyTypeEntity.getName(),
                    stpListThirdPartyTypeEntity.getState(),
                    stpListThirdPartyTypeEntity.getIdUserLog(),
                    stpListThirdPartyTypeEntity.getDatetimeLog()
            );
        }
        return stpListThirdPartyTypeDto;
    }

    /**
     *
     * Description: This method convert a third party type dto to third party type entity.
     *
     * @param stpListThirdPartyTypeDto
     * @return
     */
    public static StpListThirdPartyTypeEntity convertToEntity(StpListThirdPartyTypeDto stpListThirdPartyTypeDto) {
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new StpListThirdPartyTypeEntity();
        stpListThirdPartyTypeEntity.setIdListThirdPartyType(stpListThirdPartyTypeDto.getIdListThirdPartyType());
        stpListThirdPartyTypeEntity.setCode(stpListThirdPartyTypeDto.getCode());
        stpListThirdPartyTypeEntity.setName(stpListThirdPartyTypeDto.getName());
        stpListThirdPartyTypeEntity.setState(stpListThirdPartyTypeDto.getState());
        stpListThirdPartyTypeEntity.setIdUserLog(stpListThirdPartyTypeDto.getIdUserLog());
        stpListThirdPartyTypeEntity.setDatetimeLog(stpListThirdPartyTypeDto.getDatetimeLog());
        return stpListThirdPartyTypeEntity;
    }
}
