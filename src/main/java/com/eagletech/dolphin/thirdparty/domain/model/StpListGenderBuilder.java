package com.eagletech.dolphin.thirdparty.domain.model;

import com.eagletech.dolphin.thirdparty.application.dto.StpListGenderDto;

public class StpListGenderBuilder {

    private StpListGenderBuilder() {}

    /**
     *
     * Description: This method convert a Gender entity to Gender dto.
     *
     * @param stpListGenderEntity
     * @return
     */
    public static StpListGenderDto convertToDomain(StpListGenderEntity stpListGenderEntity) {
        StpListGenderDto stpListGenderDto = null;
        if (stpListGenderEntity != null) {
            stpListGenderDto = new StpListGenderDto(
                    stpListGenderEntity.getIdListGender(),
                    stpListGenderEntity.getCode(),
                    stpListGenderEntity.getName(),
                    stpListGenderEntity.getState(),
                    stpListGenderEntity.getIdUserLog(),
                    stpListGenderEntity.getDatetimeLog()
            );
        }
        return stpListGenderDto;
    }

    /**
     *
     * Description: This method convert a Legal Nature dto to Political Legal Nature entity.
     *
     * @param stpListGenderDto
     * @return
     */
    public static StpListGenderEntity convertToEntity(StpListGenderDto stpListGenderDto) {
        StpListGenderEntity listGenderEntity = new StpListGenderEntity();
        listGenderEntity.setIdListGender(stpListGenderDto.getIdListGender());
        listGenderEntity.setCode(stpListGenderDto.getCode());
        listGenderEntity.setName(stpListGenderDto.getName());
        listGenderEntity.setState(stpListGenderDto.getState());
        listGenderEntity.setIdUserLog(stpListGenderDto.getIdUserLog());
        listGenderEntity.setDatetimeLog(stpListGenderDto.getDatetimeLog());
        return listGenderEntity;
    }
}
