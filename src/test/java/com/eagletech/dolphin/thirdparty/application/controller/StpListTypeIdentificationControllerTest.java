package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListTypeIdentificationDto;
import com.eagletech.dolphin.thirdparty.application.service.ListTypeIdentificationService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(StpListTypeIdentificationController.class)
class StpListTypeIdentificationControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListTypeIdentificationController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListTypeIdentificationService service;

    private final String urlApi = "/third-party-api/type-identification";

    @Test
    void testShouldFindAllTypeIdentification() throws Exception {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        List<StpListTypeIdentificationEntity> identificationEntities = new ArrayList<>();
        identificationEntities.add(stpListTypeIdentificationEntity);
        Mockito.when(service.getListTypeIdentifications()).thenReturn(identificationEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllTypeIdentification() {
        //arrange
        Mockito.when(service.getListTypeIdentifications()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllTypeIdentification() {
        //arrange
        Mockito.when(service.getListTypeIdentifications()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpTypeIdentification() throws Exception {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        StpListTypeIdentificationDto stpListTypeIdentificationDto = StpListTypeIdentificationBuilder.convertToDomain(stpListTypeIdentificationEntity);
        String content = objectMapper.writeValueAsString(stpListTypeIdentificationEntity);
        Mockito.when(service.saveStpListTypeIdentification(stpListTypeIdentificationDto)).thenReturn(stpListTypeIdentificationDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpTypeIdentification() throws JsonProcessingException {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        String content = objectMapper.writeValueAsString(stpListTypeIdentificationEntity);
        Mockito.when(service.saveStpListTypeIdentification(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpTypeIdentification() throws JsonProcessingException {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        String content = objectMapper.writeValueAsString(stpListTypeIdentificationEntity);
        Mockito.when(service.saveStpListTypeIdentification(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpTypeIdentification() throws Exception {
        //arrange
        Date date = new Date();
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().buildWithDate(date);
        StpListTypeIdentificationEntity stpListTypeIdentificationEntityUpdated = new ListTypeIdentificationBuilder().buildWithDate(date);
        StpListTypeIdentificationDto stpListTypeIdentificationDto = StpListTypeIdentificationBuilder.convertToDomain(stpListTypeIdentificationEntity);
        StpListTypeIdentificationEntity listTypeIdentification = new ListTypeIdentificationBuilder().buildWithDate(date);
        String content = objectMapper.writeValueAsString(listTypeIdentification);
        String contentValidated = objectMapper.writeValueAsString(stpListTypeIdentificationEntityUpdated);
        Mockito.when(service.updateStpListTypeIdentification(stpListTypeIdentificationDto)).thenReturn(stpListTypeIdentificationDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpTypeIdentification() throws JsonProcessingException {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        String content = objectMapper.writeValueAsString(stpListTypeIdentificationEntity);
        Mockito.when(service.updateStpListTypeIdentification(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpTypeIdentification() throws JsonProcessingException {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        String content = objectMapper.writeValueAsString(stpListTypeIdentificationEntity);
        Mockito.when(service.updateStpListTypeIdentification(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindTypeIdentificationById() throws Exception {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        Mockito.when(service.getListTypeIdentificationById(Mockito.any())).thenReturn(stpListTypeIdentificationEntity);
        //act
        mockMvc.perform(get(urlApi + "/getById/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindTypeIdentificationById() {
        //arrange
        Mockito.when(service.getListTypeIdentificationById(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindTypeIdentificationById() {
        //arrange
        Mockito.when(service.getListTypeIdentificationById(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindAllTypeIdentificationWithPagination() throws Exception {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        List<StpListTypeIdentificationEntity> identificationEntities = new ArrayList<>();
        identificationEntities.add(stpListTypeIdentificationEntity);
        Page<StpListTypeIdentificationEntity> stpListTypeIdentificationPage = new PageImpl<>(identificationEntities);
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenReturn(stpListTypeIdentificationPage);
        //act
        mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllTypeIdentificationWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllTypeIdentificationWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }
}