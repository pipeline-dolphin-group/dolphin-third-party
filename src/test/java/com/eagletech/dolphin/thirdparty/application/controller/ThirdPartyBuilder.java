package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.*;

import java.util.Date;

public class ThirdPartyBuilder {
    private ThpThirdPartyEntity thpThirdPartyEntity;

    public ThpThirdPartyEntity build() {
        this.thpThirdPartyEntity = new ThpThirdPartyEntity();
        this.thpThirdPartyEntity.setIdThirdParty(1L);
        this.thpThirdPartyEntity.setNumberIdentification("test");
        this.thpThirdPartyEntity.setSurname("test");
        this.thpThirdPartyEntity.setSecondSurname("test");
        this.thpThirdPartyEntity.setFirstName("test");
        this.thpThirdPartyEntity.setMiddleName("test");
        this.thpThirdPartyEntity.setBirthdayDate(new Date());
        this.thpThirdPartyEntity.setMobile("test");
        this.thpThirdPartyEntity.setMail("test");
        this.thpThirdPartyEntity.setState(true);
        this.thpThirdPartyEntity.setUserLog(1L);
        this.thpThirdPartyEntity.setDatetimeLog(new Date());
        this.thpThirdPartyEntity.setAddress("test");
        this.thpThirdPartyEntity.setIdPoliticalDivisionCity(1L);
        this.thpThirdPartyEntity.setIdPoliticalDivisionNeighborhood(1L);
        this.thpThirdPartyEntity.setIdThirdPartyType(1L);
        this.thpThirdPartyEntity.setIdListLegalNature(1L);
        this.thpThirdPartyEntity.setIdListTypeIdentification(1L);
        this.thpThirdPartyEntity.setIdListGender(1L);
        this.thpThirdPartyEntity.setPoliticalDivisionCity(new StpPoliticalDivisionEntity());
        this.thpThirdPartyEntity.setPoliticalDivisionNeighborhood(new StpPoliticalDivisionEntity());
        this.thpThirdPartyEntity.setThirdPartyType(new StpListThirdPartyTypeEntity());
        this.thpThirdPartyEntity.setListLegalNature(new StpListLegalNatureEntity());
        this.thpThirdPartyEntity.setListTypeIdentification(new StpListTypeIdentificationEntity());
        this.thpThirdPartyEntity.setListGender(new StpListGenderEntity());
        return this.thpThirdPartyEntity;
    }

    public ThpThirdPartyEntity buildWithDate(Date date){
        this.thpThirdPartyEntity = this.build();
        this.thpThirdPartyEntity.setDatetimeLog(date);
        this.thpThirdPartyEntity.setBirthdayDate(date);
        return this.thpThirdPartyEntity;
    }
}
