package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;

import java.util.Date;

public class PoliticalDivisionBuilder {
    private StpPoliticalDivisionEntity stpPoliticalDivisionEntity;

    public StpPoliticalDivisionEntity build() {
        this.stpPoliticalDivisionEntity = new StpPoliticalDivisionEntity();
        this.stpPoliticalDivisionEntity.setIdPoliticalDivision(1L);
        this.stpPoliticalDivisionEntity.setIdPoliticalDivisionFather(1L);
        this.stpPoliticalDivisionEntity.setIdListPoliticalDivisionType(1L);
        this.stpPoliticalDivisionEntity.setIdListPoliticalDivisionGroup(1L);
        this.stpPoliticalDivisionEntity.setCode("cod_test");
        this.stpPoliticalDivisionEntity.setName("name test");
        this.stpPoliticalDivisionEntity.setState(true);
        this.stpPoliticalDivisionEntity.setIdUserLog(1L);
        this.stpPoliticalDivisionEntity.setDatetimeLog(new Date());
        this.stpPoliticalDivisionEntity.setPoliticalDivisionFather(new StpPoliticalDivisionEntity());
        this.stpPoliticalDivisionEntity.setPoliticalDivisionType(new StpListPoliticalDivisionTypeEntity());
        this.stpPoliticalDivisionEntity.setListPoliticalDivisionGroup(new StpListPoliticalDivisionGroupEntity());
        return this.stpPoliticalDivisionEntity;
    }

    public StpPoliticalDivisionEntity buildWithDate(Date date){
        this.stpPoliticalDivisionEntity = this.build();
        this.stpPoliticalDivisionEntity.setDatetimeLog(date);
        return this.stpPoliticalDivisionEntity;
    }
}
