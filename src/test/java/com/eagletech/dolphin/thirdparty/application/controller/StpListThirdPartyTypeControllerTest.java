package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListThirdPartyTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListThirdPartyTypeService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StpListThirdPartyTypeController.class)
class StpListThirdPartyTypeControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListThirdPartyTypeController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListThirdPartyTypeService service;

    private final String urlApi = "/third-party-api/third-party-type";

    @Test
    void testShouldFindAllThirdPartyTypes() throws Exception {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        List<StpListThirdPartyTypeEntity> stpListThirdPartyTypeEntities = new ArrayList<>();
        stpListThirdPartyTypeEntities.add(stpListThirdPartyTypeEntity);
        Mockito.when(service.getListThirdPartyType()).thenReturn(stpListThirdPartyTypeEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpListTypeIdentification() {
        //arrange
        Mockito.when(service.getListThirdPartyType()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpListTypeIdentification() {
        //arrange
        Mockito.when(service.getListThirdPartyType()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindAllThirdPartyTypesWithPagination() throws Exception {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        List<StpListThirdPartyTypeEntity> stpListThirdPartyTypeEntities = new ArrayList<>();
        stpListThirdPartyTypeEntities.add(stpListThirdPartyTypeEntity);
        Page<StpListThirdPartyTypeEntity> thpThirdPartyTypesPage = new PageImpl<>(stpListThirdPartyTypeEntities);
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenReturn(thpThirdPartyTypesPage);
        //act
        mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllThirdPartyTypesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllThirdPartyTypesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpListThirdPartyType() throws Exception {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        StpListThirdPartyTypeDto stpListThirdPartyTypeDto = StpListThirdPartyTypeBuilder.convertToDomain(stpListThirdPartyTypeEntity);
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        Mockito.when(service.saveStpListThirdPartyType(stpListThirdPartyTypeDto)).thenReturn(stpListThirdPartyTypeDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpListThirdPartyType() throws JsonProcessingException {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        Mockito.when(service.saveStpListThirdPartyType(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpListThirdPartyType() throws JsonProcessingException {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        Mockito.when(service.saveStpListThirdPartyType(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act - asserts
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpListThirdPartyType() throws Exception {
        //arrange
        Date date = new Date();
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().buildWithDate(date);
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntityUpdated = new ListThirdPartyTypeBuilder().buildWithDate(date);
        StpListThirdPartyTypeDto stpListThirdPartyTypeDto = StpListThirdPartyTypeBuilder.convertToDomain(stpListThirdPartyTypeEntity);
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        String contentValidated = objectMapper.writeValueAsString(stpListThirdPartyTypeEntityUpdated);
        Mockito.when(service.updateStpListThirdPartyType(stpListThirdPartyTypeDto)).thenReturn(stpListThirdPartyTypeDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpListThirdPartyType() throws JsonProcessingException {
        //arrange
        Date date = new Date();
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().buildWithDate(date);
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        Mockito.when(service.updateStpListThirdPartyType(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpListThirdPartyType() throws JsonProcessingException {
        //arrange
        Date date = new Date();
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().buildWithDate(date);
        String content = objectMapper.writeValueAsString(stpListThirdPartyTypeEntity);
        Mockito.when(service.updateStpListThirdPartyType(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }
}