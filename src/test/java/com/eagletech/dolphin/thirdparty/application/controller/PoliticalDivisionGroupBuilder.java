package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;

import java.util.Date;

public class PoliticalDivisionGroupBuilder {
    private StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity;

    public StpListPoliticalDivisionGroupEntity build() {
        this.stpListPoliticalDivisionGroupEntity = new StpListPoliticalDivisionGroupEntity();
        this.stpListPoliticalDivisionGroupEntity.setIdListPoliticalDivisionGroup(1L);
        this.stpListPoliticalDivisionGroupEntity.setCode("cod_test");
        this.stpListPoliticalDivisionGroupEntity.setName("name test");
        this.stpListPoliticalDivisionGroupEntity.setState(true);
        this.stpListPoliticalDivisionGroupEntity.setIdUserLog(1L);
        this.stpListPoliticalDivisionGroupEntity.setDatetimeLog(new Date());
        return this.stpListPoliticalDivisionGroupEntity;
    }

    public StpListPoliticalDivisionGroupEntity buildWithDate(Date date){
        this.stpListPoliticalDivisionGroupEntity = this.build();
        this.stpListPoliticalDivisionGroupEntity.setDatetimeLog(date);
        return this.stpListPoliticalDivisionGroupEntity;
    }
}
