package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;

import java.util.Date;

public class ListTypeIdentificationBuilder {
    private StpListTypeIdentificationEntity stpListTypeIdentificationEntity;

    public StpListTypeIdentificationEntity build() {
        this.stpListTypeIdentificationEntity = new StpListTypeIdentificationEntity();
        this.stpListTypeIdentificationEntity.setIdListTypeIdentification(1L);
        this.stpListTypeIdentificationEntity.setCode("cod test");
        this.stpListTypeIdentificationEntity.setName("name test");
        this.stpListTypeIdentificationEntity.setState(true);
        this.stpListTypeIdentificationEntity.setIdUserLog(1L);
        this.stpListTypeIdentificationEntity.setDatetimeLog(new Date());
        return this.stpListTypeIdentificationEntity;
    }

    public StpListTypeIdentificationEntity buildWithDate(Date date) {
        this.stpListTypeIdentificationEntity = this.build();
        this.stpListTypeIdentificationEntity.setDatetimeLog(date);
        return this.stpListTypeIdentificationEntity;
    }
}
