package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;

import java.util.Date;

public class ListGenderBuilder {

    private StpListGenderEntity listGenderEntity;

    public StpListGenderEntity build() {
        this.listGenderEntity = new StpListGenderEntity();
        this.listGenderEntity.setIdListGender(1L);
        this.listGenderEntity.setCode("cod_test");
        this.listGenderEntity.setName("name test");
        this.listGenderEntity.setState(true);
        this.listGenderEntity.setIdUserLog(1L);
        this.listGenderEntity.setDatetimeLog(new Date());
        return this.listGenderEntity;
    }

    public StpListGenderEntity buildWithDate(Date date){
        this.listGenderEntity = this.build();
        this.listGenderEntity.setDatetimeLog(date);
        return this.listGenderEntity;
    }
}
