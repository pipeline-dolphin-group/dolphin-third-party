package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionGroupDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionGroupService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(StpListPoliticalDivisionGroupController.class)
class StpListPoliticalDivisionGroupControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListPoliticalDivisionGroupController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListPoliticalDivisionGroupService service;

    private final String urlApi = "/third-party-api/political-division-group";

    @Test
    void testShouldFindAllPoliticalDivisionGroup() throws Exception {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        List<StpListPoliticalDivisionGroupEntity> stpListPoliticalDivisionGroupEntities = new ArrayList<>();
        stpListPoliticalDivisionGroupEntities.add(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.getStpListPoliticalDivisionGroups()).thenReturn(stpListPoliticalDivisionGroupEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllPoliticalDivisionType() {
        //arrange
        Mockito.when(service.getStpListPoliticalDivisionGroups()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllPoliticalDivisionGroup() {
        //arrange
        Mockito.when(service.getStpListPoliticalDivisionGroups()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpPoliticalDivisionGroup() throws Exception {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto = StpListPoliticalDivisionGroupBuilder.convertToDomain(stpListPoliticalDivisionGroupEntity);
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.saveStpListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto)).thenReturn(stpListPoliticalDivisionGroupDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpPoliticalDivisionGroup() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.saveStpListPoliticalDivisionGroup(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpPoliticalDivisionGroup() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.saveStpListPoliticalDivisionGroup(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpPoliticalDivisionGroup() throws Exception {
        //arrange
        Date date = new Date();
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().buildWithDate(date);
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntityUpdate = new PoliticalDivisionGroupBuilder().buildWithDate(date);
        StpListPoliticalDivisionGroupDto stpListPoliticalDivisionGroupDto = StpListPoliticalDivisionGroupBuilder.convertToDomain(stpListPoliticalDivisionGroupEntity);
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        String contentValidated = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntityUpdate);
        Mockito.when(service.updateStpListPoliticalDivisionGroup(stpListPoliticalDivisionGroupDto)).thenReturn(stpListPoliticalDivisionGroupDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpPoliticalDivisionGroup() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.updateStpListPoliticalDivisionGroup(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpPoliticalDivisionGroup() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionGroupEntity);
        Mockito.when(service.updateStpListPoliticalDivisionGroup(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }
}