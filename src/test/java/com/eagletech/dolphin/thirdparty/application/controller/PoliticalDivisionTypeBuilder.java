package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;

import java.util.Date;

public class PoliticalDivisionTypeBuilder {
    private StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity;

    public StpListPoliticalDivisionTypeEntity build() {
        this.stpListPoliticalDivisionTypeEntity = new StpListPoliticalDivisionTypeEntity();
        this.stpListPoliticalDivisionTypeEntity.setIdListPoliticalDivisionType(1L);
        this.stpListPoliticalDivisionTypeEntity.setCode("cod_test");
        this.stpListPoliticalDivisionTypeEntity.setName("name test");
        this.stpListPoliticalDivisionTypeEntity.setState(true);
        this.stpListPoliticalDivisionTypeEntity.setIdUserLog(1L);
        this.stpListPoliticalDivisionTypeEntity.setDatetimeLog(new Date());
        return this.stpListPoliticalDivisionTypeEntity;
    }

    public StpListPoliticalDivisionTypeEntity buildWithDate(Date date){
        this.stpListPoliticalDivisionTypeEntity = this.build();
        this.stpListPoliticalDivisionTypeEntity.setDatetimeLog(date);
        return this.stpListPoliticalDivisionTypeEntity;
    }
}
