package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpPoliticalDivisionDto;
import com.eagletech.dolphin.thirdparty.application.service.PoliticalDivisionService;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StpPoliticalDivisionController.class)
class StpPoliticalDivisionControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpPoliticalDivisionController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PoliticalDivisionService service;

    private final String urlApi = "/third-party-api/political-division";

    @Test
    void testShouldFindAllPoliticalDivision() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        List<StpPoliticalDivisionEntity> stpPoliticalDivisionEntities = new ArrayList<>();
        stpPoliticalDivisionEntities.add(stpPoliticalDivisionEntity);
        Mockito.when(service.getPoliticalDivision()).thenReturn(stpPoliticalDivisionEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllPoliticalDivision() {
        //arrange
        Mockito.when(service.getPoliticalDivision()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllPoliticalDivision() {
        //arrange
        Mockito.when(service.getPoliticalDivision()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpPoliticalDivision() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        StpPoliticalDivisionDto stpPoliticalDivisionDto = StpPoliticalDivisionBuilder.convertToDomain(stpPoliticalDivisionEntity);
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        Mockito.when(service.savePoliticalDivision(stpPoliticalDivisionDto)).thenReturn(stpPoliticalDivisionDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpPoliticalDivision() throws JsonProcessingException {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        Mockito.when(service.savePoliticalDivision(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpPoliticalDivision() throws JsonProcessingException {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        Mockito.when(service.savePoliticalDivision(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpPoliticalDivision() throws Exception {
        //arrange
        Date date = new Date();
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().buildWithDate(date);
        StpPoliticalDivisionEntity stpPoliticalDivisionEntityUpdate = new PoliticalDivisionBuilder().buildWithDate(date);
        StpPoliticalDivisionDto stpPoliticalDivisionDto = StpPoliticalDivisionBuilder.convertToDomain(stpPoliticalDivisionEntity);
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        String contentValidated = objectMapper.writeValueAsString(stpPoliticalDivisionEntityUpdate);
        Mockito.when(service.updatePoliticalDivision(stpPoliticalDivisionDto)).thenReturn(stpPoliticalDivisionDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpPoliticalDivision() throws JsonProcessingException {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        Mockito.when(service.updatePoliticalDivision(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpPoliticalDivision() throws JsonProcessingException {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        String content = objectMapper.writeValueAsString(stpPoliticalDivisionEntity);
        Mockito.when(service.updatePoliticalDivision(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindPoliticalDivisionById() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        Mockito.when(service.getPoliticalDivisionById(Mockito.any())).thenReturn(stpPoliticalDivisionEntity);
        //act
        mockMvc.perform(get(urlApi + "/getById/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindPoliticalDivisionById() {
        //arrange
        Mockito.when(service.getPoliticalDivisionById(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindPoliticalDivisionById() {
        //arrange
        Mockito.when(service.getPoliticalDivisionById(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddPoliticalDivisions() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        StpPoliticalDivisionDto stpPoliticalDivisionDto = StpPoliticalDivisionBuilder.convertToDomain(stpPoliticalDivisionEntity);
        List<StpPoliticalDivisionDto> stpPoliticalDivisionsDto = new ArrayList<>();
        stpPoliticalDivisionsDto.add(stpPoliticalDivisionDto);
        List<StpPoliticalDivisionEntity> politicalDivisionEntities = new ArrayList<>();
        politicalDivisionEntities.add(stpPoliticalDivisionEntity);
        String content = objectMapper.writeValueAsString(politicalDivisionEntities);
        Mockito.when(service.savePoliticalDivisions(stpPoliticalDivisionsDto)).thenReturn(stpPoliticalDivisionsDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/importPoliticalDivision")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assertions.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldConstraintViolationExceptionAddPoliticalDivisions() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        List<StpPoliticalDivisionEntity> politicalDivisionEntities = new ArrayList<>();
        politicalDivisionEntities.add(stpPoliticalDivisionEntity);
        String content = objectMapper.writeValueAsString(politicalDivisionEntities);
        Mockito.when(service.savePoliticalDivisions(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            MvcResult mvcResult = mockMvc.perform(post(urlApi + "/importPoliticalDivision")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldNullPointerExceptionAddPoliticalDivisions() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        List<StpPoliticalDivisionEntity> politicalDivisionEntities = new ArrayList<>();
        politicalDivisionEntities.add(stpPoliticalDivisionEntity);
        String content = objectMapper.writeValueAsString(politicalDivisionEntities);
        Mockito.when(service.savePoliticalDivisions(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            MvcResult mvcResult = mockMvc.perform(post(urlApi + "/importPoliticalDivision")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindAllPoliticalDivisionWithPagination() throws Exception {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        List<StpPoliticalDivisionEntity> politicalDivisionEntities = new ArrayList<>();
        politicalDivisionEntities.add(stpPoliticalDivisionEntity);
        Page<StpPoliticalDivisionEntity> stpPoliticalDivisionPage = new PageImpl<>(politicalDivisionEntities);
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenReturn(stpPoliticalDivisionPage);
        //act
        mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllThirdPartiesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllThirdPartiesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }
}