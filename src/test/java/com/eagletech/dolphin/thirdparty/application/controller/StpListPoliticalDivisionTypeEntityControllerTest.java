package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListPoliticalDivisionTypeDto;
import com.eagletech.dolphin.thirdparty.application.service.ListPoliticalDivisionTypeService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StpListPoliticalDivisionTypeController.class)
class StpListPoliticalDivisionTypeEntityControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListPoliticalDivisionTypeController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListPoliticalDivisionTypeService service;

    private final String urlApi = "/third-party-api/political-division-type";

    @Test
    void testShouldFindAllPoliticalDivisionType() throws Exception {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        List<StpListPoliticalDivisionTypeEntity> stpPoliticalDivisionTypeEntities = new ArrayList<>();
        stpPoliticalDivisionTypeEntities.add(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.getStpListPoliticalDivisionTypes()).thenReturn(stpPoliticalDivisionTypeEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllPoliticalDivisionType() {
        //arrange
        Mockito.when(service.getStpListPoliticalDivisionTypes()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllPoliticalDivisionType() {
        //arrange
        Mockito.when(service.getStpListPoliticalDivisionTypes()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpPoliticalDivisionType() throws Exception {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        StpListPoliticalDivisionTypeDto stpListPoliticalDivisionTypeDto = StpListPoliticalDivisionTypeBuilder.convertToDomain(stpListPoliticalDivisionTypeEntity);
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.saveStpListPoliticalDivisionType(stpListPoliticalDivisionTypeDto)).thenReturn(stpListPoliticalDivisionTypeDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpPoliticalDivisionType() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.saveStpListPoliticalDivisionType(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpPoliticalDivisionType() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.saveStpListPoliticalDivisionType(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpPoliticalDivisionType() throws Exception {
        //arrange
        Date date = new Date();
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().buildWithDate(date);
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntityUpdate = new PoliticalDivisionTypeBuilder().buildWithDate(date);
        StpListPoliticalDivisionTypeDto stpPoliticalDivisionDto = StpListPoliticalDivisionTypeBuilder.convertToDomain(stpListPoliticalDivisionTypeEntity);
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        String contentValidated = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntityUpdate);
        Mockito.when(service.updateStpListPoliticalDivisionType(stpPoliticalDivisionDto)).thenReturn(stpPoliticalDivisionDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpPoliticalDivisionType() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.updateStpListPoliticalDivisionType(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpPoliticalDivisionType() throws JsonProcessingException {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        String content = objectMapper.writeValueAsString(stpListPoliticalDivisionTypeEntity);
        Mockito.when(service.updateStpListPoliticalDivisionType(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

}