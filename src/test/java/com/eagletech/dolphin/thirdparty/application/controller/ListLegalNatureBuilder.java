package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;

import java.util.Date;

public class ListLegalNatureBuilder {

    private StpListLegalNatureEntity stpListLegalNatureEntity;

    public StpListLegalNatureEntity build() {
        this.stpListLegalNatureEntity = new StpListLegalNatureEntity();
        this.stpListLegalNatureEntity.setIdListLegalNature(1L);
        this.stpListLegalNatureEntity.setCode("cod_test");
        this.stpListLegalNatureEntity.setName("name test");
        this.stpListLegalNatureEntity.setState(true);
        this.stpListLegalNatureEntity.setIdUserLog(1L);
        this.stpListLegalNatureEntity.setDatetimeLog(new Date());
        return this.stpListLegalNatureEntity;
    }

    public StpListLegalNatureEntity buildWithDate(Date date){
        this.stpListLegalNatureEntity = this.build();
        this.stpListLegalNatureEntity.setDatetimeLog(date);
        return this.stpListLegalNatureEntity;
    }
}
