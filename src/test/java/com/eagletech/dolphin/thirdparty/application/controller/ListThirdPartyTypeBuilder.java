package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;

import java.util.Date;

public class ListThirdPartyTypeBuilder {

    private StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity;

    public StpListThirdPartyTypeEntity build() {
        this.stpListThirdPartyTypeEntity = new StpListThirdPartyTypeEntity();
        this.stpListThirdPartyTypeEntity.setIdListThirdPartyType(1L);
        this.stpListThirdPartyTypeEntity.setName("name test");
        this.stpListThirdPartyTypeEntity.setCode("code1");
        this.stpListThirdPartyTypeEntity.setState(true);
        this.stpListThirdPartyTypeEntity.setIdUserLog(1L);
        this.stpListThirdPartyTypeEntity.setDatetimeLog(new Date());
        return this.stpListThirdPartyTypeEntity;
    }

    public StpListThirdPartyTypeEntity buildWithDate(Date date) {
        this.stpListThirdPartyTypeEntity = this.build();
        this.stpListThirdPartyTypeEntity.setDatetimeLog(date);
        return this.stpListThirdPartyTypeEntity;
    }
}
