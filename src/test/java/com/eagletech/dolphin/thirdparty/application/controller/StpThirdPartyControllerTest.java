package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.ThpThirdPartyDto;
import com.eagletech.dolphin.thirdparty.application.service.ThirdPartyService;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StpThirdPartyController.class)
class StpThirdPartyControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpThirdPartyController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ThirdPartyService service;

    private final String urlApi = "/third-party-api/third-party";

    @Test
    void testShouldFindAllThirdParties() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Mockito.when(service.getThirdParty()).thenReturn(thpThirdParties);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].surname").value("test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllThirdParties() {
        //arrange
        Mockito.when(service.getThirdParty()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllThirdParties() {
        //arrange
        Mockito.when(service.getThirdParty()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindThirdPartyById() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        Mockito.when(service.getThirdPartyById(Mockito.any())).thenReturn(thpThirdPartyEntity);
        //act
        mockMvc.perform(get(urlApi + "/getById/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindThirdPartyById() {
        //arrange
        Mockito.when(service.getThirdPartyById(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindThirdPartyById() {
        //arrange
        Mockito.when(service.getThirdPartyById(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/getById/123")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddThirdParty() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        Mockito.when(service.saveThirdParty(thirdPartyDto)).thenReturn(thirdPartyDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();

        //asserts
        Assertions.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddThirdParty() throws JsonProcessingException {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        Mockito.when(service.saveThirdParty(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddThirdParty() throws JsonProcessingException {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        Mockito.when(service.saveThirdParty(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateThirdParty() throws Exception {
        //arrange
        Date date = new Date();
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().buildWithDate(date);
        ThpThirdPartyEntity thpThirdPartyEntityUpdate = new ThirdPartyBuilder().buildWithDate(date);
        ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        String contentValidated = objectMapper.writeValueAsString(thpThirdPartyEntityUpdate);
        Mockito.when(service.updateThirdParty(thirdPartyDto)).thenReturn(thirdPartyDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateThirdParty() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        Mockito.when(service.updateThirdParty(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateThirdParty() throws JsonProcessingException {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        String content = objectMapper.writeValueAsString(thpThirdPartyEntity);
        Mockito.when(service.updateThirdParty(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddThirdParties() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
        List<ThpThirdPartyDto> thpThirdPartiesDto = new ArrayList<>();
        thpThirdPartiesDto.add(thirdPartyDto);
        List<ThpThirdPartyEntity> thpThirdPartiesEntity = new ArrayList<>();
        thpThirdPartiesEntity.add(thpThirdPartyEntity);
        String content = objectMapper.writeValueAsString(thpThirdPartiesEntity);
        Mockito.when(service.saveThirdParties(thpThirdPartiesDto)).thenReturn(thpThirdPartiesDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/importThirdParties")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assertions.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddThirdParties() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
        List<ThpThirdPartyDto> thpThirdPartiesDto = new ArrayList<>();
        thpThirdPartiesDto.add(thirdPartyDto);
        String content = objectMapper.writeValueAsString(thpThirdPartiesDto);
        Mockito.when(service.saveThirdParties(thpThirdPartiesDto)).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/importThirdParties")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldNullPointerExceptionWhenAddThirdParties() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        ThpThirdPartyDto thirdPartyDto = ThpThirdPartyBuilder.convertToDomain(thpThirdPartyEntity);
        List<ThpThirdPartyDto> thpThirdPartiesDto = new ArrayList<>();
        thpThirdPartiesDto.add(thirdPartyDto);
        String content = objectMapper.writeValueAsString(thpThirdPartiesDto);
        Mockito.when(service.saveThirdParties(thpThirdPartiesDto)).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/importThirdParties")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindByNameOrIdentification() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Mockito.when(service.findByNameOrIdentification(Mockito.any())).thenReturn(thpThirdParties);
        //act - asserts
        mockMvc.perform(get(urlApi + "/findByNameOrIdentification/1234")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindByNameOrIdentification() {
        //arrange
        Mockito.when(service.findByNameOrIdentification(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findByNameOrIdentification/1234")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldNullPointerExceptionWhenFindByNameOrIdentification() {
        //arrange
        Mockito.when(service.findByNameOrIdentification(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findByNameOrIdentification/1234")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldFindAllThirdPartiesWithPagination() throws Exception {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Page<ThpThirdPartyEntity> thpThirdPartiesPage = new PageImpl<>(thpThirdParties);
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenReturn(thpThirdPartiesPage);
        //act
        mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllThirdPartiesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllThirdPartiesWithPagination() {
        //arrange
        int offset = 0, pageSize = 5;
        Mockito.when(service.findAllWithPagination(offset, pageSize)).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll/" + offset + "/" + pageSize)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }
}