package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListGenderDto;
import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;
import com.eagletech.dolphin.thirdparty.application.service.ListGenderService;
import com.eagletech.dolphin.thirdparty.application.service.ListLegalNatureService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StpListGenderController.class)
class StpListGenderControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListGenderController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListGenderService service;

    private final String urlApi = "/third-party-api/gender";

    @Test
    void testShouldFindAllListGender() throws Exception {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        List<StpListGenderEntity> stpListGenderEntities = new ArrayList<>();
        stpListGenderEntities.add(stpListGenderEntity);
        Mockito.when(service.getListGenders()).thenReturn(stpListGenderEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllListGender() {
        //arrange
        Mockito.when(service.getListGenders()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllListGender() {
        //arrange
        Mockito.when(service.getListGenders()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpListGender() throws Exception {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        StpListGenderDto stpListGenderDto = StpListGenderBuilder.convertToDomain(stpListGenderEntity);
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        Mockito.when(service.saveStpListGender(stpListGenderDto)).thenReturn(stpListGenderDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpListGender() throws JsonProcessingException {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        Mockito.when(service.saveStpListGender(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpListGender() throws JsonProcessingException {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        Mockito.when(service.saveStpListGender(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpListGender() throws Exception {
        //arrange
        Date date = new Date();
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().buildWithDate(date);
        StpListGenderEntity stpListGenderEntityUpdate = new ListGenderBuilder().buildWithDate(date);
        StpListGenderDto stpListGenderDto = StpListGenderBuilder.convertToDomain(stpListGenderEntity);
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        String contentValidated = objectMapper.writeValueAsString(stpListGenderEntityUpdate);
        Mockito.when(service.updateStpListGender(stpListGenderDto)).thenReturn(stpListGenderDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpListGender() throws JsonProcessingException {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        Mockito.when(service.updateStpListGender(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpListGender() throws JsonProcessingException {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        String content = objectMapper.writeValueAsString(stpListGenderEntity);
        Mockito.when(service.updateStpListGender(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

}