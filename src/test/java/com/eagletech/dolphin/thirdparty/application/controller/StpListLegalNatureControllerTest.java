package com.eagletech.dolphin.thirdparty.application.controller;

import com.eagletech.dolphin.thirdparty.application.dto.StpListLegalNatureDto;
import com.eagletech.dolphin.thirdparty.application.service.ListLegalNatureService;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(StpListLegalNatureController.class)
class StpListLegalNatureControllerTest {

    private static final String MESSAGE_ERROR = "Request processing failed; nested exception is java.lang.NullPointerException";

    @Autowired
    private StpListLegalNatureController controller;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ListLegalNatureService service;

    private final String urlApi = "/third-party-api/legal-nature";

    @Test
    void testShouldFindAllLegalNature() throws Exception {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        List<StpListLegalNatureEntity> stpListLegalNatureEntities = new ArrayList<>();
        stpListLegalNatureEntities.add(stpListLegalNatureEntity);
        Mockito.when(service.getListLegalNatures()).thenReturn(stpListLegalNatureEntities);
        //act
        mockMvc.perform(get(urlApi + "/findAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name test"))
                .andExpect(jsonPath("$", hasItems()))
                .andExpect(status().isOk());
    }

    @Test
    void testShouldNullPointerExceptionWhenFindAllLegalNature() {
        //arrange
        Mockito.when(service.getListLegalNatures()).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenFindAllLegalNature() {
        //arrange
        Mockito.when(service.getListLegalNatures()).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(get(urlApi + "/findAll")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldAddStpLegalNature() throws Exception {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        StpListLegalNatureDto stpListLegalNatureDto = StpListLegalNatureBuilder.convertToDomain(stpListLegalNatureEntity);
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        Mockito.when(service.saveStpListLegalNature(stpListLegalNatureDto)).thenReturn(stpListLegalNatureDto);
        //act
        MvcResult mvcResult = mockMvc.perform(post(urlApi + "/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(content, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenAddStpLegalNature() throws JsonProcessingException {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        Mockito.when(service.saveStpListLegalNature(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenAddStpLegalNature() throws JsonProcessingException {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        Mockito.when(service.saveStpListLegalNature(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(post(urlApi + "/create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldUpdateStpLegalNature() throws Exception {
        //arrange
        Date date = new Date();
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().buildWithDate(date);
        StpListLegalNatureEntity stpListLegalNatureEntityUpdate = new ListLegalNatureBuilder().buildWithDate(date);
        StpListLegalNatureDto stpListLegalNatureDto = StpListLegalNatureBuilder.convertToDomain(stpListLegalNatureEntity);
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        String contentValidated = objectMapper.writeValueAsString(stpListLegalNatureEntityUpdate);
        Mockito.when(service.updateStpListLegalNature(stpListLegalNatureDto)).thenReturn(stpListLegalNatureDto);
        //act
        MvcResult mvcResult = mockMvc.perform(put(urlApi + "/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andReturn();
        //asserts
        Assert.assertEquals(contentValidated, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testShouldNullPointerExceptionWhenUpdateStpLegalNature() throws JsonProcessingException {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        Mockito.when(service.updateStpListLegalNature(Mockito.any())).thenThrow(NullPointerException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

    @Test
    void testShouldConstraintViolationExceptionWhenUpdateStpLegalNature() throws JsonProcessingException {
        //arrange
        StpListLegalNatureEntity stpListLegalNatureEntity = new ListLegalNatureBuilder().build();
        String content = objectMapper.writeValueAsString(stpListLegalNatureEntity);
        Mockito.when(service.updateStpListLegalNature(Mockito.any())).thenThrow(ConstraintViolationException.class);
        //act
        try {
            mockMvc.perform(put(urlApi + "/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andReturn();
        } catch (Exception e) {
            //asserts
            Assertions.assertEquals(MESSAGE_ERROR, e.getMessage());
        }
    }

}