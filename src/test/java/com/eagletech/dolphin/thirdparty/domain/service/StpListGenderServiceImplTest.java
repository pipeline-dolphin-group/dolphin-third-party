package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.ListGenderBuilder;
import com.eagletech.dolphin.thirdparty.application.controller.ListLegalNatureBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListGenderEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StpListGenderServiceImplTest {

    @InjectMocks
    private StpListGenderServiceImpl service;

    @Mock
    private StpListGenderService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetLegalNature() {
        //arrange
        StpListGenderEntity stpListGenderEntity = new ListGenderBuilder().build();
        ArrayList<StpListGenderEntity> stpListGenderEntities = new ArrayList<>();
        stpListGenderEntities.add(stpListGenderEntity);
        Mockito.when(repository.findAll()).thenReturn(stpListGenderEntities);
        //act
        List<StpListGenderEntity> listGenders = service.getListGenders();
        //assert
        for (StpListGenderEntity listGenderEntity :
                listGenders) {
            assertEquals(listGenderEntity.getIdListGender(), stpListGenderEntity.getIdListGender());
        }
    }
}