package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.PoliticalDivisionGroupBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionGroupEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StpListPoliticalDivisionGroupServiceImplTest {

    @InjectMocks
    private StpListPoliticalDivisionGroupServiceImpl service;

    @Mock
    private StpListPoliticalDivisionGroupService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetPoliticalDivisionGroup() {
        //arrange
        StpListPoliticalDivisionGroupEntity stpListPoliticalDivisionGroupEntity = new PoliticalDivisionGroupBuilder().build();
        ArrayList<StpListPoliticalDivisionGroupEntity> stpListPoliticalDivisionGroupEntities = new ArrayList<>();
        stpListPoliticalDivisionGroupEntities.add(stpListPoliticalDivisionGroupEntity);
        Mockito.when(repository.findAll()).thenReturn(stpListPoliticalDivisionGroupEntities);
        //act
        List<StpListPoliticalDivisionGroupEntity> politicalDivisionGroups = service.getStpListPoliticalDivisionGroups();
        //assert
        for (StpListPoliticalDivisionGroupEntity listPoliticalDivisionGroup :
                politicalDivisionGroups) {
            assertEquals(listPoliticalDivisionGroup.getIdListPoliticalDivisionGroup(), stpListPoliticalDivisionGroupEntity.getIdListPoliticalDivisionGroup());
        }
    }
}