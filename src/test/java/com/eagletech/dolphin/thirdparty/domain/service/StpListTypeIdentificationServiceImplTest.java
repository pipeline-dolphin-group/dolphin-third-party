package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.ListTypeIdentificationBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StpListTypeIdentificationServiceImplTest {

    @InjectMocks
    private StpListTypeIdentificationServiceImpl service;

    @Mock
    private StpListTypeIdentificationService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetTypeIdentification() {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        ArrayList<StpListTypeIdentificationEntity> identificationEntities = new ArrayList<>();
        identificationEntities.add(stpListTypeIdentificationEntity);
        Mockito.when(repository.findAll()).thenReturn(identificationEntities);
        //act
        List<StpListTypeIdentificationEntity> listTypeIdentifications = service.getListTypeIdentifications();
        //assert
        for (StpListTypeIdentificationEntity listTypeIdentification :
                listTypeIdentifications) {
            assertEquals(listTypeIdentification.getIdListTypeIdentification(), stpListTypeIdentificationEntity.getIdListTypeIdentification());
        }
    }

    @Test
    void testShouldGetTypeIdentificationWithPagination() {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        ArrayList<StpListTypeIdentificationEntity> identificationEntities = new ArrayList<>();
        identificationEntities.add(stpListTypeIdentificationEntity);
        Page<StpListTypeIdentificationEntity> ListTypeIdentificationEntityPage = new PageImpl<>(identificationEntities);
        Mockito.when(repository.findAllWithPagination(0,5)).thenReturn(ListTypeIdentificationEntityPage);
        //act
        Page<StpListTypeIdentificationEntity> typeIdentificationWithPagination = service.findAllWithPagination(0, 5);
        //assert
        for (StpListTypeIdentificationEntity listTypeIdentification :
                typeIdentificationWithPagination) {
            Assertions.assertEquals(listTypeIdentification.getIdListTypeIdentification(), stpListTypeIdentificationEntity.getIdListTypeIdentification());
        }
    }

    @Test
    void testShouldGetTypeIdentificationById() {
        //arrange
        StpListTypeIdentificationEntity stpListTypeIdentificationEntity = new ListTypeIdentificationBuilder().build();
        Mockito.when(repository.findById(Mockito.any())).thenReturn(java.util.Optional.ofNullable(stpListTypeIdentificationEntity));
        //act
        StpListTypeIdentificationEntity listTypeIdentificationById = service.getListTypeIdentificationById(Mockito.any());
        //assert
        Assertions.assertEquals(listTypeIdentificationById.getIdListTypeIdentification(), stpListTypeIdentificationEntity.getIdListTypeIdentification());
    }
}