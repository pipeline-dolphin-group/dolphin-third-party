package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.ThirdPartyBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.ThpThirdPartyEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

class ThpThirdPartyServiceImplTest {

    @InjectMocks
    private ThpThirdPartyServiceImpl service;

    @Mock
    private ThpThirdPartyService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetThirdParty() {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Mockito.when(repository.findAll()).thenReturn(thpThirdParties);
        //act
        List<ThpThirdPartyEntity> thirdParties = service.getThirdParty();
        //assert
        for (ThpThirdPartyEntity thpThirdParty :
                thirdParties) {
            Assertions.assertEquals(thpThirdParty.getIdThirdParty(), thpThirdPartyEntity.getIdThirdParty());
        }
    }

    @Test
    void testShouldGetThirdPartyWithPagination() {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Page<ThpThirdPartyEntity> thpThirdPartiesPage = new PageImpl<>(thpThirdParties);
        Mockito.when(repository.findAllWithPagination(0, 5)).thenReturn(thpThirdPartiesPage);
        //act
        Page<ThpThirdPartyEntity> thirdPartyWithPagination = service.findAllWithPagination(0, 5);
        //assert
        for (ThpThirdPartyEntity thpThirdParty :
                thirdPartyWithPagination) {
            Assertions.assertEquals(thpThirdParty.getIdThirdParty(), thpThirdPartyEntity.getIdThirdParty());
        }
    }

    @Test
    void testShouldFindByNameOrIdentification() {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        List<ThpThirdPartyEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        String param = "carlos";
        Mockito.when(repository.findByNumberIdentificationOrSurname(param)).thenReturn(thpThirdParties);
        //act
        List<ThpThirdPartyEntity> byNameOrIdentification = service.findByNameOrIdentification(param);
        //assert
        for (ThpThirdPartyEntity thpThirdParty :
                byNameOrIdentification) {
            Assertions.assertEquals(thpThirdParty.getIdThirdParty(), thpThirdPartyEntity.getIdThirdParty());
        }
    }

    @Test
    void testShouldGetThirdPartyById() {
        //arrange
        ThpThirdPartyEntity thpThirdPartyEntity = new ThirdPartyBuilder().build();
        Mockito.when(repository.findById(Mockito.any())).thenReturn(java.util.Optional.ofNullable(thpThirdPartyEntity));
        //act
        ThpThirdPartyEntity thirdPartyById = service.getThirdPartyById(Mockito.any());
        //assert
        Assertions.assertEquals(thirdPartyById.getIdThirdParty(), thpThirdPartyEntity.getIdThirdParty());
    }
}