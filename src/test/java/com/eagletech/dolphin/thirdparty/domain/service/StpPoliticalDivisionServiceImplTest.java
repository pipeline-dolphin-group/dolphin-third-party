package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.PoliticalDivisionBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpPoliticalDivisionEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StpPoliticalDivisionServiceImplTest {

    @InjectMocks
    private StpPoliticalDivisionServiceImpl service;

    @Mock
    private StpPoliticalDivisionService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetPoliticalDivision() {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        ArrayList<StpPoliticalDivisionEntity> stpPoliticalDivisionEntities = new ArrayList<>();
        stpPoliticalDivisionEntities.add(stpPoliticalDivisionEntity);
        Mockito.when(repository.findAll()).thenReturn(stpPoliticalDivisionEntities);
        //act
        List<StpPoliticalDivisionEntity> politicalDivision = service.getPoliticalDivision();
        //assert
        for (StpPoliticalDivisionEntity politicalDivisionEntity :
                politicalDivision) {
            assertEquals(politicalDivisionEntity.getIdPoliticalDivision(), stpPoliticalDivisionEntity.getIdPoliticalDivision());
        }
    }

    @Test
    void testShouldGetPoliticalDivisionWithPagination() {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        ArrayList<StpPoliticalDivisionEntity> stpPoliticalDivisionEntities = new ArrayList<>();
        stpPoliticalDivisionEntities.add(stpPoliticalDivisionEntity);
        Page<StpPoliticalDivisionEntity> politicalDivisionEntityPage = new PageImpl<>(stpPoliticalDivisionEntities);
        Mockito.when(repository.findAllWithPagination(0, 5)).thenReturn(politicalDivisionEntityPage);
        //act
        Page<StpPoliticalDivisionEntity> politicalDivisionWithPagination = service.findAllWithPagination(0, 5);
        //assert
        for (StpPoliticalDivisionEntity politicalDivision :
                politicalDivisionWithPagination) {
            Assertions.assertEquals(politicalDivision.getIdPoliticalDivision(), stpPoliticalDivisionEntity.getIdPoliticalDivision());
        }
    }

    @Test
    void testShouldGetPoliticalDivisionById() {
        //arrange
        StpPoliticalDivisionEntity stpPoliticalDivisionEntity = new PoliticalDivisionBuilder().build();
        Mockito.when(repository.findById(Mockito.any())).thenReturn(java.util.Optional.ofNullable(stpPoliticalDivisionEntity));
        //act
        StpPoliticalDivisionEntity politicalDivisionById = service.getPoliticalDivisionById(Mockito.any());
        //assert
        Assertions.assertEquals(politicalDivisionById.getIdPoliticalDivision(), stpPoliticalDivisionEntity.getIdPoliticalDivision());
    }
}