package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.ListThirdPartyTypeBuilder;
import com.eagletech.dolphin.thirdparty.application.controller.ListTypeIdentificationBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListThirdPartyTypeEntity;
import com.eagletech.dolphin.thirdparty.domain.model.StpListTypeIdentificationEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

class StpListThirdPartyTypeServiceImplTest {

    @InjectMocks
    private StpListThirdPartyTypeServiceImpl service;

    @Mock
    private StpListThirdPartyTypeService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetThirdPartyType() {
        //arrange
        StpListThirdPartyTypeEntity thpThirdPartyEntity = new ListThirdPartyTypeBuilder().build();
        List<StpListThirdPartyTypeEntity> thpThirdParties = new ArrayList<>();
        thpThirdParties.add(thpThirdPartyEntity);
        Mockito.when(repository.findAll()).thenReturn(thpThirdParties);
        //act
        List<StpListThirdPartyTypeEntity> thirdPartyTypes = service.getListThirdPartyType();
        //assert
        for (StpListThirdPartyTypeEntity thpThirdPartyType :
                thirdPartyTypes) {
            Assertions.assertEquals(thpThirdPartyType.getIdListThirdPartyType(), thpThirdPartyEntity.getIdListThirdPartyType());
        }
    }

    @Test
    void testShouldGetTypeIdentificationWithPagination() {
        //arrange
        StpListThirdPartyTypeEntity stpListThirdPartyTypeEntity = new ListThirdPartyTypeBuilder().build();
        ArrayList<StpListThirdPartyTypeEntity> listThirdPartyType = new ArrayList<>();
        listThirdPartyType.add(stpListThirdPartyTypeEntity);
        Page<StpListThirdPartyTypeEntity> listThirdPartyTypeEntityPage = new PageImpl<>(listThirdPartyType);
        Mockito.when(repository.findAllWithPagination(0,5)).thenReturn(listThirdPartyTypeEntityPage);
        //act
        Page<StpListThirdPartyTypeEntity> stpListThirdPartyTypeEntityPage = service.findAllWithPagination(0, 5);
        //assert
        for (StpListThirdPartyTypeEntity stpListThirdPartyType :
                stpListThirdPartyTypeEntityPage) {
            Assertions.assertEquals(stpListThirdPartyType.getIdListThirdPartyType(), stpListThirdPartyTypeEntity.getIdListThirdPartyType());
        }
    }
}