package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.PoliticalDivisionTypeBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListPoliticalDivisionTypeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StpListPoliticalDivisionTypeEntityServiceImplTest {

    @InjectMocks
    private StpListPoliticalDivisionTypeServiceImpl service;

    @Mock
    private StpListPoliticalDivisionTypeService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetPoliticalDivisionType() {
        //arrange
        StpListPoliticalDivisionTypeEntity stpListPoliticalDivisionTypeEntity = new PoliticalDivisionTypeBuilder().build();
        ArrayList<StpListPoliticalDivisionTypeEntity> stpPoliticalDivisionTypeEntities = new ArrayList<>();
        stpPoliticalDivisionTypeEntities.add(stpListPoliticalDivisionTypeEntity);
        Mockito.when(repository.findAll()).thenReturn(stpPoliticalDivisionTypeEntities);
        //act
        List<StpListPoliticalDivisionTypeEntity> politicalDivisionType = service.getStpListPoliticalDivisionTypes();
        //assert
        for (StpListPoliticalDivisionTypeEntity listPoliticalDivisionType :
                politicalDivisionType) {
            assertEquals(listPoliticalDivisionType.getIdListPoliticalDivisionType(), stpListPoliticalDivisionTypeEntity.getIdListPoliticalDivisionType());
        }
    }
}