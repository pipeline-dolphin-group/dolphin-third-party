package com.eagletech.dolphin.thirdparty.domain.service;

import com.eagletech.dolphin.thirdparty.application.controller.ListLegalNatureBuilder;
import com.eagletech.dolphin.thirdparty.domain.model.StpListLegalNatureEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StpListLegalNatureServiceImplTest {

    @InjectMocks
    private StpListLegalNatureServiceImpl service;

    @Mock
    private StpListLegalNatureService repository;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testShouldGetLegalNature() {
        //arrange
        StpListLegalNatureEntity stpListLegalNature = new ListLegalNatureBuilder().build();
        ArrayList<StpListLegalNatureEntity> stpListLegalNatureEntities = new ArrayList<>();
        stpListLegalNatureEntities.add(stpListLegalNature);
        Mockito.when(repository.findAll()).thenReturn(stpListLegalNatureEntities);
        //act
        List<StpListLegalNatureEntity> listLegalNatures = service.getListLegalNatures();
        //assert
        for (StpListLegalNatureEntity listLegalNatureEntity :
                listLegalNatures) {
            assertEquals(listLegalNatureEntity.getIdListLegalNature(), stpListLegalNature.getIdListLegalNature());
        }
    }
}